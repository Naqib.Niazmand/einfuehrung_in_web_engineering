<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 9.1: Komponente in Vue.js</h2>
        <p><span>Schreiben Sie eine </span><a target="_blank" href="https://vuejs.org/v2/guide/components.html" rel="noopener"><em>Vue.js Single File Component</em></a><span> mit einem Text-Eingabefeld und 3 Ausgabefeldern, in denen man während des Tippens sehen kann, (a) wie viele Buchstaben (b) wie viele Leerzeichen und (c) wie viele Worte man in das Text-Eingabefeld bereits eingegeben hat. </span></p>
		
		<p>Betten Sie Ihre Komponente in eine Webseite zweimal ein und testen Sie, ob beide Komponenten unabhängig voneinander sind.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie Ihre Vue.js Single File Component hier ein:</p>


<p class="TextBlock">
 https://codesandbox.io/s/gallant-meadow-s84r8?file=/src/components/EingabeMitAusgabe.vue:391-822
 main.js wurde automatisch generiert von codesandbox.io
 Die Komponente EingabeMitAusgabe.vue und App.vue habe ich selbst geschrieben. bei App.vue ist der  
 < style > Tag automatisch generiert von codesandbox.io 
 <br>
 Für den Ergebnis füge ich  ein Bild hinzu. Code sieht man weiter unten <br>
<img src="Uebung9_1.PNG" class="Image" alt="Uebung 9.1 ErgebnisBild"> 
</p>
<xmp class="TextBlock">
 main.js:
 import Vue from "vue";
 import App from "./App.vue";
 
 Vue.config.productionTip = false;
 
 new Vue({
 &emsp;	render: h => h(App)
 }).$mount("#app");
 
 EingabeMitAusgabe.vue:
 
 <template>
 <div class="EingabeAusgabe">
   <input type="text" v-model="eingabe" @keydown="setCount" />
   <p>{{ buchstaben }}</p>
   <p>{{ leerzeichen }}</p>
   <p>{{ worte }}</p>
 </div>
 </template>
 
 <script>
 export default {
	name: "EingabeMitAusgabe",
	data: function () {
		return {
		buchstaben: 0,
		leerzeichen: 0,
		worte: 0,
		};
	},  
	methods: {
		setCount: function () {
		let str = this.eingabe.toString();
		let nurBuchstaben = 0;
		let leerzeichenAnzahl = str.split(" ").length - 1;
      
		if(leerzeichenAnzahl > 0){
			nurBuchstaben = str.length - leerzeichenAnzahl;
		}else{
			nurBuchstaben = str.length;
		}
		this.buchstaben = nurBuchstaben;
		this.leerzeichen = leerzeichenAnzahl;
		this.worte = leerzeichenAnzahl;
		},
	},
 };
 </script> 
</xmp>
	<p class="description">Geben Sie die Webseite, auf der Sie Ihre Komponente mehrfach testen, hier ein:</p>
<xmp class="TextBlock">
 App.vue: 	
 
 <template>
 <div id="app">
   <EingabeMitAusgabe />
   <EingabeMitAusgabe />
   <EingabeMitAusgabe />
 </div>
 </template>
 
 <script>
 import EingabeMitAusgabe from "./components/EingabeMitAusgabe";
 
 export default {
   name: "App",
   components: {
     EingabeMitAusgabe
   }
 };
 </script>
 
 <style>
 #app {
   font-family: "Avenir", Helvetica, Arial, sans-serif;
   -webkit-font-smoothing: antialiased;
   -moz-osx-font-smoothing: grayscale;
   text-align: center;
   color: #2c3e50;
   margin-top: 60px;
 }
 </style>
	
	</xmp>
    </div>
<?php include ("./includes/footer.php"); ?>