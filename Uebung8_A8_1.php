<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>8.1. Promises</h2>
        
		<div class="panel-heading"><p>Erstellen Sie auf Ihrem Server www2.inf.h-brs.de (oder localhost) zwei Text-Dateien A.txt und B.txt mit ungefähr gleich vielen Zeilen. Laden Sie mit der fetch-API parallel beide Text-Dateien vom Server. Geben Sie auf einer Webseite den Inhalt beider Dateien zeilenweise aus, wobei der Anfang der Zeile aus A.txt und das Ende aus B.txt stammen soll. Die beiden Dateien sollen also zeilenweise konkateniert werden. Erzielen Sie max. Geschwindigkeit durch maximale Parallelität. Achten Sie gleichzeitig auf Korrektheit. Verwenden Sie dabei ausschließlich die Promise API ohne async / await.</p></div>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie die komplette HTML-Seite für die Promise-Lösung inkl. JavaScript-Quelltext an:</p>
		
        <script>
			// Arrays zum zwischen Spichern
            var textA = [];
            var textB = [];
            Promise.all([
				// hole jede Zeile aus der Datei A.txt und speichere es in Array: textA
                fetch('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/A.txt').then( response => {
                    return response.text().then( text => {
                        text.split("\n").forEach(zeile => {
                            textA.push( zeile);
                        })
                    });
                }),
				// hole jede Zeile aus der Datei B.txt und speichere es in Array: textB
                fetch('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/B.txt').then( response => {
                    return response.text().then( text => {
                        text.split("\n").forEach(zeile => {
                            textB.push( zeile);
                        })
                    });
                })
            ]).then(result => { 
			/* wenn beide Arrays gefüllt sind, dann Konkatiniere die Zeilen aus textA mit den Zeilen aus TextB
				und Prüfe welche Datei weniger Zeilen hat. */
				let minZeilen = 0;
				if(textA.length < textB.length){
					minZeilen = textA.length;
				}else{
					minZeilen = textB.length;
				}
                for(i = 0; i < minZeilen; i++){
                    document.getElementById("ZeigeKombiTexte").insertAdjacentHTML('beforeend', textA[i] + " " + textB[i] + "<br>");
                }
            }) .catch(error => console.log(`Error in promises ${error}`));

        </script>
		
        <div id="ZeigeKombiTexte"></div>
		
		<p class="TextBlock">
		
			// Arrays zum zwischen Spichern<br>
            var textA = [];<br>
            var textB = [];<br>
            Promise.all([<br>
			&emsp;&emsp;	// hole jede Zeile aus der Datei A.txt und speichere es in Array: textA<br>
            &emsp;&emsp;    fetch('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/A.txt').then( response => {<br>
            &emsp;&emsp;        &emsp;return response.text().then( text => {<br>
            &emsp;&emsp;        &emsp;    &emsp; text.split("\n").forEach(zeile => {<br>
            &emsp;&emsp;        &emsp;    &emsp;   &emsp;  textA.push( zeile);<br>
            &emsp;&emsp;        &emsp;    &emsp; })<br>
            &emsp;&emsp;        &emsp;});<br>
            &emsp;&emsp;    }),<br>
			&emsp;&emsp;	// hole jede Zeile aus der Datei B.txt und speichere es in Array: textB<br>
            &emsp;&emsp;    fetch('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/B.txt').then( response => {<br>
            &emsp;&emsp;       &emsp; return response.text().then( text => {<br>
            &emsp;&emsp;       &emsp;   &emsp;  text.split("\n").forEach(zeile => {<br>
            &emsp;&emsp;       &emsp;   &emsp;    &emsp;  textB.push( zeile);<br>
            &emsp;&emsp;       &emsp;   &emsp;  })<br>
            &emsp;&emsp;       &emsp; });<br>
            &emsp;&emsp;    })<br>
            ]).then(result => { <br>
			/* wenn beide Arrays gefüllt sind, dann Konkatiniere die Zeilen aus textA mit den Zeilen aus TextB<br>
				und Prüfe welche Datei weniger Zeilen hat. */<br>
				&emsp; let minZeilen = 0;<br>
				&emsp; if(textA.length < textB.length){<br>
				&emsp; &emsp; 	minZeilen = textA.length;<br>
				&emsp; }else{<br>
				&emsp; &emsp;	minZeilen = textB.length;<br>
				&emsp; }<br>
                &emsp; for(i = 0; i < minZeilen; i++){
                document.getElementById("ZeigeKombiTexte").insertAdjacentHTML('beforeend', textA[i] + " " + textB[i]);<br>
                &emsp; }<br>
            }) .catch(error => console.log(`Error in promises ${error}`));<br>
		</p>
		
    </div>
<?php include ("./includes/footer.php"); ?>