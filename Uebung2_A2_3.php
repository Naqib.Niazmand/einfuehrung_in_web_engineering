<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>

        <div class="task">
            <h1>Beschreibung der Aufgabe</h1>
            <h2>2.3. Wireframe with HTML and CSS</h2>
            <p>Gegeben ist folgendes HTML-Wireframe:</p>
			<img alt="Wireframe of a Survey form" src="https://kaul.inf.h-brs.de/we/assets/img/survey.png" width="100%">
			<p><strong>Hinweis zu den Farben:</strong> Firefox DevTools anthalten eine Pipette namens "Eyedropper", mit der man die Farben aus einer Webseite auslesen kann. Die grünliche Farbe im Rahmen des Wireframes ist demnach #b2d6d1,<br><br>
			<strong>Hinweis zu den Schriften (Fonts): </strong>Die größte Sammlung an "kostenlosen" Web-Schriften stammt von Google und ist unter <a target="_blank" href="https://fonts.google.com/" rel="noopener">Google Fonts</a> zu finden. Wer lieber aus dem Vorrat an vorhanden Schriften auswählt, wird unter <a target="_blank" href="https://www.zeichenschatz.net/typografie/websichere-schriften.html" rel="noopener">websichere Schriften</a> fündig.<br><br>
			
			<strong>Frage:</strong> Mit welchem HTML- und CSS-Code kann man diesen Wireframe exakt nachbilden?
			Schreiben Sie Ihren CSS-Code direkt in die HTML-Datei. Geben Sie Ihren HTML- und CSS-Quellcode zusammen in dieses Textfeld ein:</p>
			
			<p class="description">Was war in der Lösung dieser Aufgabe die größte Schwierigkeit und wie haben Sie diese gelöst?</p>
            
        </div>
        <div class="solution">
            <h1>Lösung der Aufgabe</h1>
            <!DOCTYPE html>

<html>
<head>
    <style>
        
        .solution {
            background-color: #b2d6d1;
            font-family: 'Roboto', sans-serif;
        }

        .form {
            background-color: rgb(245,245,245);
            box-sizing: border-box;
            margin: 2em;
            margin-top: 1em;
            width: calc(100% - 4em);
        }

        .form .centered {
            text-align: center;
        }

        .form .section {
            display: inline-block;
            box-sizing: border-box;
            padding: .25em;
            width: calc(100% - 4em);
        }

        .form .section .left {
            float: left;
            text-align: right;
            padding: .5em;
            padding-right: 0;
            box-sizing: border-box;
            width: calc(50% - 1em);
        }

        .form .section .right {
            float: right;
            text-align: left;
            padding: .5em;
            padding-left: 0;
            box-sizing: border-box;
            width: calc(50% - 1em);
        }

        select {
            padding: .5em;
            width: 150px;
            background-color: rgb(235,235,235);
        }

        input[type="text"],input[type="email"],input[type="number"]{
            width: 150px;
            margin: -0.25em;
            padding: 0.25em;
            margin-left: 0;
        }

        input[type="radio"],input[type="checkbox"]{
            margin-bottom: 0.5em;
            margin-left: 0;
        }

    </style>
</head>
<body>
    <span style="text-align: center;"><h1>Survey Form</h1></span>

    <div class="form">
        <div class="centered" style="padding: 1.5em;">
            Let us know how we can improve freeCodeCamp
        </div>

        <div class="section">
            <div class="left">
                . Name:
            </div>
            <div class="right">
                <input type="text" placeholder="Enter your name"></input>
            </div>
        </div>

        <div class="section">
            <div class="left">
                . Email:
            </div>
            <div class="right">
                <input type="email" placeholder="Enter your Email"></input>
            </div>
        </div>

        <div class="section">
            <div class="left">
                . Age:
            </div>
            <div class="right">
                <input type="number" placeholder="Age"></input>
            </div>
        </div>

        <div class="section">
            <div class="left">
                Which option best describes your current role?
            </div>
            <div class="right">
                <select name="role" id="role">
                    <option value="student">Student</option>
                    <option value="werkstudent">Werkstudent</option>
                    <option value="praktikant">Praktikant</option>
                  </select>
            </div>
        </div>

        <div class="section">
            <div class="left">
                . How likely is that you would recommend freeCodeCampt to a friend?
            </div>
            <div class="right">
                <input type="radio" id="RadioDef" name="recommend" value="definitely">
                <label for="RadioDef"> Definitely</label> <br>
                <input type="radio" id="RadioMay" name="recommend" value="maybe">
                <label for="RadioMay"> Maybe</label> <br>
                <input type="radio" id="RadioNot" name="recommend" value="notesure">
                <label for="RadioNot"> Not Sure</label> 
            </div>
        </div>

        <div class="section">
            <div class="left">
                What do you like the most in FCC:
            </div>
            <div class="right">
                <select name="ilike" id="ilike">
                    <option selected="selected" value="null">Select an option</option>
                    <option value="option 1">option 1</option>
                    <option value="option 2">option 2</option>
                    <option value="option 3">option 3</option>
                </select>
            </div>
        </div>

        <div class="section">
            <div class="left">
                Things that should be improved in the future <br> (Check all that apply):
            </div>
            <div class="right">
                <input type="checkbox" id="CheckImproveFront">
                <label for="CheckImproveFront"> Front-end Projects</label> <br>
                <input type="checkbox" id="CheckImproveBack">
                <label for="CheckImproveBack"> Back-end Projects</label> <br>
                <input type="checkbox" id="CheckImproveData">
                <label for="CheckImproveData"> Data Visualization</label> 
            </div>
        </div>
		<p class="description">Was war in der Lösung dieser Aufgabe die größte Schwierigkeit und wie haben Sie diese gelöst?</p>
        <p class="TextBlock">Die Strukturierung der einzelne Elemente Responsive hinzubekommen.</p>
    </div>
</body>
</html>
        </div>

<?php include ("./includes/footer.php"); ?>
