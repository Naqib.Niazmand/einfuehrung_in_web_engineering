<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 9.2: Menü-Komponente</h2>
        <p><span>Schreiben Sie eine möglichst flexible </span><a target="_blank" href="https://vuejs.org/v2/guide/components.html" rel="noopener"><em>Vue.js Single File Component</em></a><span> für Menüs und wenden Sie diese in derselben Webseite zweimal an, einmal horizontal, das andere Mal vertikal.</span></p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie die Inhalte aller Dateien Ihrer Lösung inkl. JS-Quelltext hintereinander ein. Schreiben Sie vor jede Datei deren Dateiname:</p>
        <p class="TextBlock">
		<br>
		Für den Ergebnis füge ich  ein Bild hinzu. Code sieht man weiter unten <br>
		<img src="Uebung9_2.PNG" class="Image" alt="Uebung 9.2 ErgebnisBild"> 
		</p>
		
<xmp class="TextBlock">
//die main.ja wurde von Vue CLI generiert.

main.js:
import { createApp } from 'vue'
import App from './App.vue'
createApp(App).mount('#app')

// von mir 
HorizentalOrVerticalMenu.vue:
<template>
  <div class="menuItems">
    <ul>
      <li><a href="#"> Menu Item 1 </a></li>
      <li><a href="#"> Menu Item 2 </a></li>
      <li><a href="#"> Menu Item 3 </a></li>
      <li><a href="#"> Menu Item 4 </a></li>
      <li><a href="#"> Menu Item 5 </a></li>
      <li><a href="#"> Menu Item 6 </a></li>
    </ul>
  </div>
</template>
<script>
export default {
  name: 'menuItems',
  props: {menuItems: { type: String, default: "Menu Item"} },
}
</script>

<style scoped>

.horizontal {
  flex-direction: row;
  justify-content: center;
}

.vertikal {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
}

.menuItems  {
  display: flex;
}

.menuItems ul {
  list-style: none;
  display: inherit;
  flex-direction: inherit;
}

.menuItems ul li a {
  color: black;
  margin: .5em;
  width: 100px;
  font-family: "Arial Black";
  background-color: #eee;
  display: block;
  border: solid black;
  text-decoration: none;
  padding: 16px 16px 16px 64px;
  box-shadow: 6px 6px 6px 0 #202020;
  border: thin solid black;
  border-radius: 1rem;
}

</style>

App.vue:
// von mir. style tag wurde automatisch generiert von Vue CLI
<template>
  <div id="app">
    <EingabeMitAusgabe />
    <HorizentalOrVerticalMenu v-bind:class="{horizontal: true}"></HorizentalOrVerticalMenu>
    <HorizentalOrVerticalMenu v-bind:class="{vertikal: true}"></HorizentalOrVerticalMenu>
  </div>
</template>

<script>
import EingabeMitAusgabe from "./components/EingabeMitAusgabe";
import HorizentalOrVerticalMenu from './components/HorizentalOrVerticalMenu.vue'


export default {
  name: "App",
  components: {
    EingabeMitAusgabe,
    HorizentalOrVerticalMenu,
  }
};
</script>

<style>
#app {
  font-family: "Avenir", Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>

</xmp>

    </div>
<?php include ("./includes/footer.php"); ?>