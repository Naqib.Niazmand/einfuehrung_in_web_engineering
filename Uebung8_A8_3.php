<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>8.3. WWW-Navigator</h2>
        
		
		<div class="panel-heading"><p><span>Schreiben Sie einen Navigator für die Fachbegriffe des WWW zu den Oberthemen HTML, CSS und JavaScript. Im Hauptmenü sollen diese 3 Oberthemen zur Auswahl stehen. Im Untermenü soll eine zugehörige Liste von Fachbegriffen zum jeweiligen ausgewählten Oberthema stehen. In der Mitte soll eine Dokumentation zum ausgewählten Fachbegriff erscheinen </span><s>mit Hyperlinks zu den anderen Fachbegriffen. Wird auf einen solchen Hyperlink geklickt, so sollen sich auch die beiden Menüs anpassen. Mit dem Back-Button des Browsers soll ein Zurücksprung möglich sein.</s></p><div style="text-align: center;"><img src="www-navigator.png" alt="WWW-Navigator" width="80%" height="auto"></div><p>Schreiben Sie in HTML und CSS nur den responsiven Rahmen für einen solchen WWW-Navigator. Dabei können Sie auch ein schöneres Layout als das hier gezeigte erstellen. Die Inhalte sollen in einer JSON-Datei extern gelagert werden. Mit der fetch-API soll die JSON-Datei asynchron nicht-blockierend geladen werden, und zwar nur einmal, nicht mehrfach. (d.h. Sparen Sie Internet-Bandbreite.) Sobald die Inhalte angekommen sind, sollen sie im Browser auch sofort angezeigt werden.</p><p>Fügen Sie selbst in die JSON-Datei zusätzliche Inhalte zu Themen der Vorlesung als Strings beispielhaft ein. Wenn Sie Inhalte aus fremden Quellen kopieren, so schreiben Sie bitte stets die Quelle als externe Ressource hinzu. Diese soll dann auf der rechten Seite im WWW-Navigator erscheinen.</p><p><span>Beispiel für die JSON-Datei (auch zum </span><a target="_blank" href="https://kaul.inf.h-brs.de/we/assets/js/navigator_contents.json" rel="noopener">Download</a><span>):</span></p><source src="/ccmjs/mkaul-components/highlight/versions/ccm.highlight-3.0.1.js"><ccm-highlight-3-0-1 clazz="json" content="{
  &quot;html&quot;: {
    &quot;headings&quot;: {
      &quot;content&quot;: &quot;Die Überschriftenelemente bestehen aus sechs verschiedenen Leveln. <h1> ist die Überschrift mit der höchsten Gewichtung und <h6> mit der kleinsten. Ein Überschriften-Element beschreibt das Thema des Bereiches, welcher der Überschrift folgt. Überschriften können auch verwendet werden, um ein Inhaltsverzeichnis für ein Dokument automatisch zu erstellen.&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Web/HTML/Element/h1-h6&quot;]
    },
    &quot;paragraph&quot;: {
      &quot;content&quot;: &quot;Das <p>-Element erzeugt einen Absatz, den zusammenhängenden Abschnitt eines längeren Textes. In HTML kann <p> jedoch für jedwede Art von zu gruppierendem, zusammenhängendem Inhalt genutzt werden, zum Beispiel Bilder oder Formularfelder.&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Web/HTML/Element/p&quot;]
    }
  },
  &quot;css&quot;: {
    &quot;selectors&quot;: {
      &quot;content&quot;: &quot;Selektoren definieren, auf welche Elemente eine Reihe von CSS Regeln angewendet wird.&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Web/CSS/CSS_Selectors&quot;]
    },
    &quot;colors&quot;: {
      &quot;content&quot;: &quot;Der CSS Datentyp Color beschreibt eine Farbe im sRGB Farbraum. Eine Farbe kann auf eine von drei Arten beschrieben werden: Schlüsselworte, rgb und rgba, hsl und hsla. ...&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Web/CSS/Farben&quot;]
    }
  },
  &quot;javascript&quot;: {
    &quot;function&quot;: {
      &quot;content&quot;: &quot;Funktionen sind ein Grundbaustein in JavaScript. Eine Funktion ist eine Prozedur - eine Reihe von Anweisungen, um eine Aufgabe auszuführen oder eine Wert auszurechnen. Um Funktionen zu verwenden, müssen diese im Scope (Gültigkeitsbereich) deklariert werden, in dem sie ausgeführt werden soll.&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Funktionen&quot;]
    },
    &quot;object&quot;: {
      &quot;content&quot;: &quot;Ein Objekt ist eine Sammlung von zusammenhängenden Daten und/oder Funktionalitäten. Diese bestehen üblicherweise aus verschiedenen Variablen und Funktionen, die Eigenschaften und Methoden genannt werden, wenn sie sich innerhalb von Objekten befinden.&quot;,
      &quot;references&quot;: [&quot;https://developer.mozilla.org/de/docs/Learn/JavaScript/Objects/Basics&quot;]
    }
  }
}"><div id="highlight-3-0-1-3"></div></ccm-highlight-3-0-1></div>

        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
	
<!DOCTYPE html>
<html>
<head>
    <title>www Navi</title>
    <meta name=viewport content=width=device-width, initial-scale=1.0, user-scalable=yes>
    <meta charset=UTF-8>
    <style>
        .solution {
            font-family: Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
            min-height: 5vh;
            max-height: 65vh;
        }

        .containerU8 {
            padding: 0;
            margin: 0;
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
        }

        .headerU8 {
            flex-direction: row;
            flex: 100%;
            height: 10%;
            text-align: center;
            background-color: rgb(180, 86, 83);
            display: flex;
            color: white;
            align-items: center;
            justify-content: center;
        }

        .navigation {
            display: flex;
            flex-direction: row;
            flex: 100%;
            height: 10%;
            text-align: center;
            background-color: rgb(180, 86, 83);
            color: white;
            justify-content: flex-start;
            align-items: center;
        }
        .navigation a {
            display: flex;
            text-decoration: none;
            color: white;
            height: 100%;
            width: 100%;
            justify-content: center;
            align-items: center;
        }

        a:hover{
            background-color: black;
        }

        .content {
            display: flex;
            flex-direction: row;
            flex: 100%;
            height: 70%;
            background-color: #D8D8D8;
        }

        .menuU8 {
            display: flex;
            overflow:auto;
            flex-direction: column;
            flex: 20%;
            background-color: rgb(185, 133, 132);
			color: white;
        }

        .menuU8 a {
            background-color: #eee;
            color: black;
            display: block;
            border: solid black;
            text-decoration: none;
            padding: 8px 8px 8px 32px;
        }

        .menuU8 a:hover {
            background-color: #4CAF50;
        }

        .menuU8 ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 200px;
            background-color: #f1f1f1;
        }

        .menuU8 li  {
            display: block;
            padding: 8px 16px;
            text-align: center;
            border-bottom: 1px solid #555;
            border-top: 1px solid #555;
            border-right: 1px solid #555;
            border-left: 1px solid #555;
            text-decoration: none;
        }

        .menuU8 li a {
            display: block;
        }

        .menuU8 li a:hover {
            background-color: #555;
            color: white;
        }

        .descriptionU8{
            flex-direction: column;
            flex: 60%;
            background-color: rgb(162, 208, 240);
            color: white;
        }

        .additionalInfo {
            flex-direction: column;
            flex: 20%;
            background-color: rgb(185, 133, 132);
            color: white;
        }

        .footerU8 {
            flex: 100%;
            height: 15%;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            color: white;
            background-color: rgb(0, 0, 0);
        }

        .footerU8 a{
            color: white;
            margin-right: 15px;
        }
    </style>

</head>
<body>
    <script>

        async function fetchJson(file){
            let response;
            let jsObject;
            try{
                response = await fetch(file);
                jsObject = await response.json();
            } catch(error) {
                console.log(error);
            }
            return jsObject;
        }

        function removeAllChildNodes(parent) {
            while (parent.firstChild) {
                  parent.removeChild(parent.firstChild);
            }
        }

        function display(string){

            // Get menuU8, descriptionU8 and addional Info
            
            const parent = document.getElementById(menuU8);
            const descriptionU8 = document.getElementById(descri);
            const addionalInfo = document.getElementById(addInfo);

            // Refresh display

            removeAllChildNodes(parent);
            descriptionU8.textContent = ;
            addionalInfo.textContent = ;

            if(string === HTML) {

                // Display the categories only once

                if(parent.hasChildNodes()){
                    return;
                }

                const headings = document.createElement(li);
                const paragraph = document.createElement(li);

                headings.textContent = heading;
                paragraph.textContent = paragraph;
                let toggle = ;

                headings.onclick = () => {

                    if(toggle != headings){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        paragraph.style.color = white;
                    }

                    toggle = headings;

                    // Display selected node
                    
                    headings.style.color = blue;

                    // Update descriptionU8

                    data.then(text => { descriptionU8.textContent = text.html.headings.content;})

                    // Update addional Info

                    data.then(text => { addionalInfo.textContent = text.html.headings.references;})
                }

                paragraph.onclick = () => {

                    if(toggle != paragraph){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        headings.style.color = white;
                    }

                    toggle = paragraph;

                    // Display selected node
                   
                    paragraph.style.color = blue;

                    //Update descriptionU8

                    data.then(text => { descriptionU8.textContent = text.html.paragraph.content;})

                    //Update addional Info

                    data.then(text => { addionalInfo.textContent = text.html.paragraph.references;})
                }

                parent.appendChild(headings);
                parent.appendChild(paragraph);

                //Select category: headings by default

                headings.onclick();
            }


            else if(string === CSS) {

                //Display the categories only once

                if(parent.hasChildNodes()){
                    return;
                }

                const selectors = document.createElement(li);
                const colors = document.createElement(li);

                selectors.textContent = selectors;
                colors.textContent = colors;

                let toggle = ;

                selectors.onclick = () => {

                    if(toggle != selectors){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        colors.style.color = white;
                    }

                    toggle = selectors;

                    //Display selected node
                    
                    selectors.style.color = blue;

                    //Update descriptionU8

                    data.then(text => { descriptionU8.textContent = text.css.selectors.content;})

                    //Update addional Info

                    data.then(text => { addionalInfo.textContent = text.css.selectors.references;})
                }

                colors.onclick = () => {

                    if(toggle != colors){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        selectors.style.color = white;
                    }

                    toggle = colors;
                    
                    //Display selected node
                    
                    colors.style.color = blue;

                    //Update descriptionU8

                    data.then(text => { descriptionU8.textContent = text.css.colors.content;})

                    //Update addional Info

                    data.then(text => { addionalInfo.textContent = text.css.colors.references;})
                }

                parent.appendChild(selectors);
                parent.appendChild(colors);

                //Select category: selectors by default

                selectors.onclick();

            }


            else if(string === JavaScript) {


                // Display the categories only once

                if(parent.hasChildNodes()){
                    return;
                }

                const _function = document.createElement(li);
                const object = document.createElement(li);

                _function.textContent = function;
                object.textContent = object;

                let toggle = ;

                _function.onclick = () => {

                    if(toggle != function){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        object.style.color = white;
                    }

                    toggle = function;

                    // Display selected node
                    
                    _function.style.color = blue;

                    // Update descriptionU8

                    data.then(text => { descriptionU8.textContent = text.javascript.function.content;})

                    // Update addional Info

                    data.then(text => { addionalInfo.textContent = text.javascript.function.references;})
                }

                object.onclick = () => {

                    if(toggle != object){
                        descriptionU8.textContent = ;
                        addionalInfo.textContent = ;
                        _function.style.color = white;
                    }

                    toggle = object;

                    // Display selected node
                    
                    object.style.color = blue;

                    // Update descriptionU8
                    
                    data.then(text => { descriptionU8.textContent = text.javascript.object.content;})

                    // Update addional Info

                    data.then(text => { addionalInfo.textContent = text.javascript.object.references;})
                }

                parent.appendChild(_function);
                parent.appendChild(object);

                // Select category: selectors by default

                _function.onclick();

            }
        }

        // Fetch Data from Server
        const data = fetchJson(http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/navigator_contents.json);

    </script>
<div class=containerU8>
    <div class=headerU8>
        <h1>WWW-Navigator</h1>
    </div>
    <div class=navigation>
        <a id=html href=#html onclick = display(HTML)>HTML</a>
        <a id=css href=#css onclick = display(CSS)>CSS</a>
        <a id=javascript href=#JavaScript onclick = display(JavaScript)>JavaScript</a>
        <a id=other href=#Other onclick = display(Other)>Other</a>
    </div>
    <div class=content>
        <div class=menuU8>
            <h1>menuU8</h1>
            <lu id = menuU8></lu>
        </div>
        <div class=descriptionU8>
            <h1>descriptionU8</h1>
            <p id=descri></p>
        </div>
        <div class=additionalInfo>
            <h1>Additional Info</h1>
            <a id=addInfo></a>
        </div>
    </div>
    <div class=footerU8>
        <h1>Footer:</h1>
        <a href=#url>sitemap</a>
        <a href=#url>Home</a>
        <a href=#url>News</a>
        <a href=#url>Contact</a>
        <a href=#url>About</a>
    </div>
</div>
</body>
</html>

		
		
    </div>
<?php include ("./includes/footer.php"); ?>