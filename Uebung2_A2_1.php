<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>2.1. CSS Selektoren und CSS Farben</h2>
        <p>Berechnen Sie die Spezifität folgender CSS-Selektoren:</p>
		<ol>
			<li>div div div:focus  .inner</li>
			<li>h1 + div.main</li>
			<li>div a:link[href*='h-brs']</li>
			<li>nav > a:hover::before</li>
			<li>ul#primary-nav  li.active</li>
		</ol>
		
		<p>Rechnen Sie folgende RGB-Werte in HSL-Werte um:</p>
		<ol>
			<li>#ffffff</li>
			<li>#000</li>
			<li>#ab0978</li>
			<li>rgb(127,255,33)</li>
			<li>rgba(255,127,33,0.8)</li>
		</ol>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description"> Berechnen Sie die Spezifität folgender CSS-Selektoren <br>
			<ol class="description">
				<li>div div div:focus  .inner</li>
				<li>h1 + div.main</li>
				<li>div a:link[href*='h-brs']</li>
				<li>nav > a:hover::before</li>
				<li>ul#primary-nav  li.active</li>
			</ol> 
		</p>
		<ol class="TextBlock">
			<li>1 + 1 + 10 + 10 = 22</li>
			<li>1 + 1 + 10 = 12</li>
			<li>1 + 1 + 10 + 10 = 22</li>
			<li>1 + 1 + 10 + 1 = 13</li>
			<li>1 + 100 + 1 + 1 + 10 = 113</li>
		</ol>
		<p class="description"> Rechnen Sie folgende RGB-Werte in HSL-Werte um: Hex->HSL<br>
			<ol class="description">
				<li>#ffffff</li>
				<li>#000</li>
				<li>#ab0978</li>
				<li>rgb(127,255,33)</li>
				<li>rgba(255,127,33,0.8)</li>
			</ol> 
		</p>
		<ol class="TextBlock">
			<li>#ffffff -> 0,0,100</li>
			<li>#000 -> 0,0,0</li>
			<li>#ab0978 -> 319,90,35</li>
			<li>rgb(127,255,33) -> 95,100,56</li>
			<li>rgba(255,127,33,0.8) -> 25,100,56</li>
		</ol>	
    </div>

<?php include ("./includes/footer.php"); ?>
