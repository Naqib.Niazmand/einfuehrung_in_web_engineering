<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>8.2. async / await</h2>
        
		<p>Lösen Sie die letzte Aufgabe mit async / await statt Promise.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie die komplette HTML-Seite für die Promise-Lösung inkl. JavaScript-Quelltext an:</p>
	
          <script>
			// Arrays zum zwischen Spichern
            var textA = [];
            var textB = [];
            async function fetchText( url ){
                const request = await fetch(url);
                return await request.text();
            }
            (async _=> {
                await Promise.all([
                    (async () => {
						// hole jede Zeile aus der Datei A.txt und speichere es in Array: textA
                        let text = await fetchText('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/A.txt');
                        text.split("\n").forEach(zeile => {
                            textA.push( zeile);
                        });
                    })(),
                    (async () => {
						// hole jede Zeile aus der Datei B.txt und speichere es in Array: textB
                        let text = await fetchText('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/B.txt');
                        text.split("\n").forEach(zeile => {
                            textB.push( zeile);
                        });
                    })(),
                ]);
                /* wenn beide Arrays gefüllt sind, dann Konkatiniere die Zeilen aus textA mit den Zeilen aus TextB
				und Prüfe welche Datei weniger Zeilen hat. */
				let minZeilen = 0;
				if(textA.length < textB.length){
					minZeilen = textA.length;
				}else{
					minZeilen = textB.length;
				}
                for(i = 0; i < minZeilen; i++){
                    document.getElementById("ZeigeKombiTexte").insertAdjacentHTML('beforeend', textA[i] + " " + textB[i] + "<br>");
                }
            })();
        </script>
		
        <div id="ZeigeKombiTexte"></div>
		
		<p class="TextBlock">
		
			// Arrays zum zwischen Spichern<br>
            var textA = [];<br>
            var textB = [];<br>
            async function fetchText( url ){<br>
            &emsp;    const request = await fetch(url);<br>
            &emsp;    return await request.text();<br>
            }<br>
            (async _=> {<br>
            &emsp;    await Promise.all([<br>
            &emsp;     &emsp;   (async () => {<br>
			&emsp;		&emsp; 	  &emsp; // hole jede Zeile aus der Datei A.txt und speichere es in Array: textA<br>
            &emsp;     &emsp;     &emsp; let text = await fetchText('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/A.txt');<br>
            &emsp;     &emsp;     &emsp; text.split("\n").forEach(zeile => {<br>
            &emsp;     &emsp;     &emsp;   &emsp;   textA.push( zeile);<br>
            &emsp;     &emsp;     &emsp; });<br>
            &emsp;     &emsp;   })(),<br>
            &emsp;     &emsp;   (async () => {<br>
			&emsp;		&emsp; 	   &emsp; // hole jede Zeile aus der Datei B.txt und speichere es in Array: textB<br>
            &emsp;     &emsp;      &emsp; let text = await fetchText('http://www2.inf.h-bonn-rhein-sieg.de/~nniazm2s/B.txt');<br>
            &emsp;     &emsp;      &emsp; text.split("\n").forEach(zeile => {<br>
            &emsp;     &emsp;      &emsp;   &emsp;  textB.push( zeile);<br>
            &emsp;     &emsp;      &emsp; });<br>
            &emsp;     &emsp;   })(),<br>
            &emsp;    ]);<br>
            &emsp;    /* wenn beide Arrays gefüllt sind, dann Konkatiniere die Zeilen aus textA mit den Zeilen aus TextB
			&emsp;	und Prüfe welche Datei weniger Zeilen hat. */<br>
			&emsp;	let minZeilen = 0;<br>
			&emsp;	if(textA.length < textB.length){<br>
			&emsp;	&emsp;	minZeilen = textA.length;<br>
			&emsp;	}else{<br>
			&emsp;	&emsp;	minZeilen = textB.length;<br>
			&emsp;	}<br>
            &emsp;    for(i = 0; i < minZeilen; i++){<br>
            &emsp;     &emsp;  document.getElementById("ZeigeKombiTexte").insertAdjacentHTML('beforeend', textA[i] + " " + textB[i]);<br>
            &emsp;    }<br>
            })();<br>
		</p>
		
    </div>
<?php include ("./includes/footer.php"); ?>