<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>5.4. Proxy</h2>
        <p>Erweitern Sie Ihre Vorrang-Klasse um Logging, indem Sie ein <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy" rel="noopener">Proxy</a> einfügen. Lassen Sie sich vom Logger bei jedem Schritt ausgeben, wie viele der Vorrangrelationen noch übrig bleiben. Verwenden Sie so weit wie möglich High-Level-Methoden wie <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Object/keys" rel="noopener">Object.keys</a> und High-Level-Datenstrukturen wie <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map" rel="noopener">Map</a> und <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set" rel="noopener">Set</a> und deren Methoden, anstatt mühsam von Hand zu iterieren und zu zählen.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie hier Ihren ECMAScript-Code inkl. Tests ein. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">

//Lösungshinweise zu Aufgabe 5.4. aus <a href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">https://kaul.inf.h-brs.de/we/#app-content-6-3t</a> übernommen<br><br>
class Vorrang { <br>
 &emsp; constructor( list, log = true ) {<br>
 &emsp;   this._predecessors = {};<br>
 &emsp;   const all = new Set();<br>
 &emsp;   for ( const [ pre, post ] of list ){<br>
 &emsp;   &emsp; all.add( pre ); all.add( post );<br>
 &emsp;   &emsp; if ( ! this._predecessors[ post ] ) <br>
&emsp;   &emsp; this._predecessors[ post ] = [];<br>
 &emsp;   &emsp; this._predecessors[ post ].push( pre );<br>
 &emsp;   }<br>
 &emsp;   this._max = all.size;<br>
 &emsp;   all.forEach( item => {<br>
 &emsp;     if ( ! this._predecessors[ item ] ) <br>
&emsp;   &emsp; this._predecessors[ item ] = [];<br>
 &emsp;   })<br>
 &emsp;   if ( log ) this._predecessors = new Proxy( <br>
 &emsp;   &emsp; this._predecessors, this.logger() )<br>
 &emsp; }<br>
 &emsp; logger() {<br>
 &emsp;   &emsp; const that = this;<br>
 &emsp;   &emsp; return {<br>
 &emsp;   &emsp;  &emsp; get( target, prop, receiver ) {<br>
 &emsp;   &emsp;  &emsp;   const count = Object.keys( that._predecessors ).length;<br>
 &emsp;   &emsp;  &emsp;   console.log( `COUNT ${prop} ${count}` );<br>
 &emsp;   &emsp;  &emsp;   console.assert( count > 0 && count <= that._max, count );<br>
 &emsp;   &emsp;  &emsp;   return target[ prop ]<br>
 &emsp;   &emsp;  &emsp; },<br>
 &emsp;   &emsp;  &emsp; has(target, prop) {<br>
 &emsp;   &emsp;  &emsp;   console.log( "HAS " + prop );<br>
 &emsp;   &emsp;  &emsp;   return prop in target;<br>
 &emsp;   &emsp;  &emsp; }<br>
 &emsp;   &emsp; }<br>
 &emsp; }<br>
 &emsp; predecessors( item ){<br>
 &emsp;  &emsp;  return this._predecessors[ item ];<br>
 &emsp; }<br>
 &emsp; * [Symbol.iterator]() {<br>
 &emsp;  &emsp; while ( Object.keys( this._predecessors ).length > 0 ){<br>
 &emsp;  &emsp;  &emsp; const find = Object.entries( this._predecessors )<br>
 &emsp;  &emsp;  &emsp; .find( ([ item, pres ]) => pres.length === 0 );<br>
 &emsp;  &emsp;  &emsp; const next = find[ 0 ];<br>
 &emsp;  &emsp;  &emsp; delete this._predecessors[ next ];<br>
 &emsp;  &emsp;  &emsp; for ( const [ item, pres ] of Object.entries( <br>
 &emsp;  &emsp;  &emsp; &emsp;	this._predecessors ) ){<br>
 &emsp;  &emsp;  &emsp;  &emsp; this._predecessors[ item ] = pres.filter( <br>
 &emsp;  &emsp;  &emsp;  &emsp; x => x !== next );<br>
 &emsp;  &emsp;  &emsp; }<br>
 &emsp;  &emsp;  &emsp; yield next;<br>
 &emsp;  &emsp; }<br>
 &emsp; }<br>
}<br>
<br>
const studentenLeben = new Vorrang([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]);<br>
console.assert( studentenLeben.predecessors( "studieren" ).includes( "schlafen" ) );<br>
console.assert( studentenLeben.predecessors( "studieren" ).includes( "essen" ) );<br>
<br>
const reihenfolge = [...studentenLeben];<br>
console.assert( reihenfolge[0] === "schlafen" );<br>
console.assert( reihenfolge[1] === "essen" );<br>
console.assert( reihenfolge[2] === "studieren" );<br>
console.assert( reihenfolge[3] === "prüfen" );<br>
		
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>