<?PHP
if ( isset($_POST[ 'benutzername' ]) && isset($_POST[ 'passwort' ]) ){
    $regBenutzername = $_POST[ 'benutzername' ];
    $regPasswort = $_POST[ 'passwort' ];
    $datei = './eingegebenenDatenU10.csv';
    $regDaten = $regBenutzername . ',' . $regPasswort . "\n";
		/*
		file_put_contents: Schreibt Daten in eine Datei	
		LOCK_EX: um eine exklusive Sperre (Schreiber) zu erhalten.
		FILE_APPEND: Falls die Datei filename bereits existiert, 
		füge die Daten an die Datei an anstatt diese zu überschreiben.
		
		https://www.php.net/manual/de/function.header.php
		header ( string $header , bool $replace = true , int $http_response_code = ? ) : void
		header — Sendet einen HTTP-Header in Rohform		*/
		
    if ( file_put_contents( $datei, $regDaten, FILE_APPEND | LOCK_EX ) ){ 
        header("Location: registrierungErfolgreich.php");
    }
}
?>