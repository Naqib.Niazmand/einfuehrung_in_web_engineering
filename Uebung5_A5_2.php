<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
	<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			border-radius: 4px;
		}
		
		.box {
			border: thin solid black;
			border-radius: 1rem;
			padding: 2rem;
			margin: 1rem;
			font-size: x-large;
			font-weight: bolder;
			width: fit-content;
			box-shadow: 4px 4px 3px 0 #777777;
		}
	</style>
	
        <h1>Beschreibung der Aufgabe</h1>
        <h2>5.2. Topologische Iterierbarkeit</h2>
        <p>Stellen Sie bei Ihrer Klasse aus der letzten Aufgabe die topologische Iterierbarkeit her (zunächst über das Iterationsprotokoll, ohne Generator, ohne yield).</p>
        <p>Zum Beispiel soll dadurch folgende <code>for ... of loop </code> möglich werden, mit der die Elemente in topologischer Sortierung durchlaufen werden:</p>
        <p>
			<code>const studentenLeben = new Vorrang([ ["schlafen", "studieren"],<br>
			["essen", "studieren"], ["studieren", "prüfen"] ]);<br><br>

			for ( const next of studentenLeben ){<br>
				&emsp;&emsp;console.log( next );<br>
			}
			</code>
		</p>
		<p>Auf der Console wird dadurch ausgegeben:</p>
		<div class="box code"><span> schlafen</span><br><span> essen</span><br><span> studieren</span><br><span> prüfen</span><br></div>
		<p>Wählen Sie eine Implementierung, die universell gültig, also nicht nur für dieses Beispiel gilt.</p>
		
		<p>Die topologische Sortierung im Konstruktor vorzuberechnen, wäre eine triviale Lösung, bei der man einfach die Lösung von 4.4 abschreibt. Versuchen Sie stattdessen, erst beim Aufruf von <code>next()</code> die erforderliche Berechnung durchzuführen, allerdings mit minimalem Aufwand.</p>
		
		<p>Verwenden Sie so weit wie möglich High-Level-Methoden wie <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Object/keys" rel="noopener">Object.keys</a> und High-Level-Datenstrukturen wie <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map" rel="noopener">Map</a> und <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set" rel="noopener">Set</a> und deren Methoden, anstatt mühsam von Hand zu iterieren und zu zählen.</p>
		
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie hier Ihren ECMAScript-Code inkl. Tests ein. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">
		
			class Vorrang {<br>
				&emsp;constructor(arr) {<br>
				&emsp;	this.arr = arr;<br>
				&emsp;}<br><br>
			
				&emsp;topsort() {<br>
				&emsp;	var ordnung = [];<br>
				&emsp;	var poscurrent = 0;<br>
				&emsp;	var posnext = 0;<br>
				&emsp;	//Bei leere Relation<br>
				&emsp;	if(this.arr[0][0] === "") {<br>
				&emsp;		return;<br>
				&emsp;	}<br>
					// Alle Tasks in der Relation werden einmalig in einer Liste aufgenommen<br>
				&emsp;	for(let x=0; x < this.arr.length; x++) {<br>
				&emsp;		&emsp; var xpostask = this.arr[x][0];<br>
				&emsp;		&emsp; var ypostask = this.arr[x][1];<br>
				&emsp;		&emsp; if(!ordnung.includes(xpostask)) {<br>
				&emsp;		&emsp; &emsp;	ordnung.push(xpostask);<br>
				&emsp;		&emsp; }<br>
				&emsp;		&emsp; if(!ordnung.includes(ypostask)) {<br>
				&emsp;		&emsp; &emsp;	ordnung.push(ypostask);<br>
				&emsp;		&emsp; }<br>
				&emsp;	}<br>
					//Nun Sortiere die Liste nach Abhängigkeiten der Relation<br>
				&emsp;	var fertig = false;<br>
				&emsp;	var tasktmp;<br>
				&emsp;	var countIterations = 0;<br>
				&emsp;	while (!fertig && countIterations < this.arr.length) {<br>
				&emsp;		&emsp;fertig = true;<br>
				&emsp;		&emsp;countIterations++;<br>
				&emsp;		&emsp;for(let x=0;x<this.arr.length;x++) {<br>  
				&emsp;		&emsp;	var xpostask = this.arr[x][0];<br>
				&emsp;		&emsp;	var ypostask = this.arr[x][1];<br>
				&emsp;		&emsp;	poscurrent = ordnung.indexOf(xpostask);<br>
				&emsp;		&emsp;	posnext = ordnung.indexOf(ypostask);<br>
				&emsp;		&emsp;	if(poscurrent > posnext) {<br>
				&emsp;		&emsp;		&emsp; //swap task<br>
				&emsp;		&emsp;		&emsp; tasktmp = ordnung[poscurrent];<br>
				&emsp;		&emsp;		&emsp; ordnung[poscurrent] = ypostask;<br>
				&emsp;		&emsp;		&emsp; ordnung[posnext] = tasktmp;<br>
				&emsp;		&emsp;		&emsp; fertig = false;<br>
				&emsp;		&emsp;	}<br>    
				&emsp;		&emsp;}<br>    
				&emsp;	}<br>
				&emsp;	if(countIterations == this.arr.length) {<br>
				&emsp;	&emsp;	ordnung = new Array();<br>
				&emsp;	}<br>
				&emsp;	&emsp; return ordnung;<br>
				&emsp;}<br>
				
				&emsp;[Symbol.iterator]() {<br>
				&emsp;	&emsp;const that = this.topsort(this.arr);<br>
				&emsp;	&emsp;let i = 0;<br>
				&emsp;	&emsp;return {<br>
				&emsp;	&emsp;	&emsp;next() {<br>
				&emsp;	&emsp;	&emsp;	&emsp;return {<br>
				&emsp;	&emsp;	&emsp;	&emsp;	&emsp;value: that[i++],<br>
				&emsp;	&emsp;	&emsp;	&emsp;	&emsp;done: i > that.length<br>
				&emsp;	&emsp;	&emsp;	&emsp;}<br>
				&emsp;	&emsp;	&emsp;}<br>
				&emsp;	&emsp;}<br>
				&emsp;}<br>
			}<br>
			<br>
            const studentenLeben = new Vorrang([ ["schlafen", "studieren"],<br> 
			["essen", "studieren"], ["studieren", "prüfen"] ]);<br>
			<br>
            // Testen Positv<br>	
			for ( const next of studentenLeben ){<br>
            &emsp;   console.log( next );<br>
			&emsp;	document.write(" " +next +" ");<br>
            }<br>
			<br>
			// Testen Positv<br>	
		
			var expected = ["schlafen", "essen", "studieren", "prüfen"];<br>
			let i = 0;
			for ( const next of studentenLeben ){<br>
			&emsp;	console.assert( expected[i++] === next,<br> 
			&emsp;	{errorMsg: "Fehler: Werte sind ungleich -> <br>
			&emsp;	Erwartung nicht erfuellt"});<br>	
			}<br>
			<br>
			// Testen Negativ<br>
			var expected2 = ["schlafen", "essen", "prüfen", "studieren"];<br>
			i=0;<br>
			for ( const next of studentenLeben ){<br>
			&emsp;	console.assert( expected2[i++] === next, <br>
			&emsp;	{expect: expected2[i], actual:next, <br>
			&emsp;	errorMsg: "Fehler: Werte sind ungleich -> <br>
			&emsp;	Erwartung nicht erfuellt"});<br>	
			}<br>
		
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>