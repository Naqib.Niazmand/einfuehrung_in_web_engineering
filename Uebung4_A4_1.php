<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>4.1. Funktionen</h2>
        <p>Schreiben Sie folgende Funktionen in JavaScript:</p>
        <p>Schreiben Sie eine Funktion identity(), die ein Argument als Parameter entgegen nimmt und dieses als Ergebnis zurück gibt.</p>
        <p>Schreiben Sie eine Funktion identity_function(), die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt.</p>
        <p>Schreiben Sie zwei binäre Funktionen add und mul, die Summe und Produkt berechnen.</p>
        <p>Schreiben Sie eine Addier-Funktion addf(), so dass addf(x)(y) genau x + y zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. addf(x) liefert eine Funktion, die auf y angewandt wird.)</p>
        <p>Schreiben Sie eine Funktion applyf(), die aus einer binären Funktion wie add(x,y) eine Funktion addfberechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. addf = applyf(add); addf(x)(y) soll add(x,y) liefern. Entsprechend applyf(mul)(5)(6) soll 30 liefern, wenn mul die binäre Multiplikation ist.</p>
		
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Schreiben Sie eine Funktion identity(), die ein Argument als Parameter entgegen nimmt und dieses als Ergebnis zurück gibt.</p>
        <p class="TextBlock"> <code> function identity(x){ <br>
					&emsp; return x;<br>
				}</code>
		</p>
		<p class="description">Schreiben Sie eine Funktion identity_function(), die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt.</p>
		<p class="TextBlock"> <code> function identity_function(x){ <br>
        	&emsp; return function(){ <br>
            		&emsp; &emsp; return x;<br>
                &emsp; };<br> 
        }</code>
		</p>
		
		<p class="description">Schreiben Sie zwei binäre Funktionen add und mul, die Summe und Produkt berechnen.</p>
		<p class="TextBlock"> 
			<code>
				function add(x,y){<br>
					&emsp;return x+y;<br>
				}<br>
				function mul(x,y){<br>
					&emsp;return x*y;<br>
				}
			</code>
		</p>
		
		<p class="description">Schreiben Sie eine Addier-Funktion addf(), so dass addf(x)(y) genau x + y zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. addf(x) liefert eine Funktion, die auf y angewandt wird.)</p>
		<p class="TextBlock"> 
			<code>
				function addf(x){<br>
				&emsp;	return function(y){<br>
					&emsp; &emsp;	return x+y;<br>
				&emsp;	};<br>
				}
			</code>
		</p>
		
		<p class="description">Schreiben Sie eine Funktion applyf(), die aus einer binären Funktion wie add(x,y) eine Funktion addfberechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. addf = applyf(add); addf(x)(y) soll add(x,y) liefern. Entsprechend applyf(mul)(5)(6) soll 30 liefern, wenn mul die binäre Multiplikation ist.</p>
		<p class="TextBlock"> 
			<code>
				function add(a, b) {<br>
				&emsp;	return a+b;<br>
				}<br>
		
				function mul(a, b) {<br>
				&emsp;	return a*b;<br>
				}<br>
		
				function applyf(arg){<br>
				&emsp;	return function(x){<br>
					&emsp; &emsp;	return function (y){<br>
						&emsp; &emsp; &emsp;	return arg(x,y);<br>
					&emsp; &emsp;	};<br>
				&emsp;	};<br>
				}
			</code>
		</p>
		
    </div>
<?php include ("./includes/footer.php"); ?>
