<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>


    <div class="task">
	<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			background-color: #f9f2f4;
			border-radius: 4px;
		}
	</style>
        <h1>Beschreibung der Aufgabe</h1>
        <h2>5.1. Klasse für Vorrangrelationen</h2>
        <p>Schreiben Sie eine ES6-Klasse Vorrang für Vorrangrelationen, z.B. 
		<code>
			new Vorrang([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ])
		</code>.Wählen Sie eine Implementierung, die universell gültig, also nicht nur für dieses Beispiel gilt. (Überlegen Sie sich, über welche Properties und Methoden eine solche Klasse verfügen sollte und wie TopSort hier hinein spielt. Topologische Iterierbarkeit und topologischer Generator sind jedoch Gegenstand der nächsten Übungen weiter unten auf diesem Übungsblatt und sollten für diese Aufgaben aufgespart werden.)<br><br>
		Verwenden Sie die neuen Sprach-Konzepte aus der Vorlesung so weit wie möglich.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie den ES6-Code inkl. Tests hier ein. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">
		
		class Vorrang {<br>
	&emsp;&emsp;constructor(arr) {<br>
	&emsp;&emsp;&emsp;	this.arr = arr;<br>
	&emsp;&emsp;}<br>
	&emsp;&emsp; topsort() {<br>
	&emsp;&emsp;	&emsp;var ordnung = [];<br>
	&emsp;&emsp;	&emsp;var poscurrent = 0;<br>
	&emsp;&emsp;	&emsp;var posnext = 0;<br>
	&emsp;&emsp;	&emsp;//Bei leere Relation<br>
	&emsp;&emsp;	&emsp;if(this.arr[0][0] === "") {<br>
	&emsp;&emsp;	&emsp;&emsp;	return;<br>
	&emsp;&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;// Alle Tasks in der Relation werden einmalig in einer Liste aufgenommen<br>
	&emsp;&emsp;	&emsp;for(let x=0; x < this.arr.length; x++) {<br>
	&emsp;&emsp;	&emsp;	&emsp;var xpostask = this.arr[x][0];<br>
	&emsp;&emsp;	&emsp;	&emsp;var ypostask = this.arr[x][1];<br>
	&emsp;&emsp;	&emsp;	&emsp;if(!ordnung.includes(xpostask)) {<br>
	&emsp;&emsp;	&emsp;	&emsp;&emsp;	ordnung.push(xpostask);<br>
	&emsp;&emsp;	&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;	&emsp;if(!ordnung.includes(ypostask)) {<br>
	&emsp;&emsp;	&emsp;	&emsp;&emsp;	ordnung.push(ypostask);<br>
	&emsp;&emsp;	&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;//Nun Sortiere die Liste nach Abhängigkeiten der Relation<br>
	&emsp;&emsp;	&emsp;var fertig = false;<br>
	&emsp;&emsp;	&emsp;var tasktmp;<br>
	&emsp;&emsp;	&emsp;var countIterations = 0;<br>
	&emsp;&emsp;	&emsp;while (!fertig && countIterations < this.arr.length) {<br>
	&emsp;&emsp;	&emsp;	&emsp;fertig = true;<br>
	&emsp;&emsp;	&emsp;	&emsp;countIterations++;<br>
	&emsp;&emsp;	&emsp;	&emsp;for(let x=0; x < this.arr.length; x++) {<br>   
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; var xpostask = this.arr[x][0];<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; var ypostask = this.arr[x][1];<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; poscurrent = ordnung.indexOf(xpostask);<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; posnext = ordnung.indexOf(ypostask);<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; if(poscurrent > posnext) {<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; 	&emsp; //swap task<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; 	&emsp; tasktmp = ordnung[poscurrent];<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; 	&emsp; ordnung[poscurrent] = ypostask;<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; 	&emsp; ordnung[posnext] = tasktmp;<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; 	&emsp; fertig = false;<br>
	&emsp;&emsp;	&emsp;	&emsp;	&emsp; }<br>
	&emsp;&emsp;	&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;if(countIterations == this.arr.length) {<br>
	&emsp;&emsp;	&emsp;	&emsp;	 ordnung = new Array();<br>
	&emsp;&emsp;	&emsp;}<br>
	&emsp;&emsp;	&emsp;return ordnung;<br>
	&emsp;&emsp;}<br>
			}<br><br>
		
			// Testen Positv<br>	
			let obj = new Vorrang([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]);<br>
			let test = obj.topsort();<br>
			var expected = ["schlafen", "essen", "studieren", "prüfen"];<br>
			for(let i = 0; i < expected.length; i++){<br>
			&emsp;&emsp;	console.assert( expected[i] === test[i], {errorMsg: "Fehler: Werte sind ungleich -> Erwartung nicht erfuellt"});<br>	
			}<br><br>
			
			// Testen Negativ<br>
			var expected2 = ["schlafen", "essen", "prüfen", "studieren"];<br>
			for(let i = 0; i < expected2.length; i++){<br>
				&emsp;&emsp;console.assert( expected2[i] === test[i], {expect: expected2[i], actual:test[i], errorMsg: "Fehler: Werte sind ungleich -> Erwartung nicht erfuellt"});<br>	
			}<br>
			document.write("Ergebnis: "+ test);<br>
		
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>