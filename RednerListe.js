var intervalForTimer = null;
function formatTime(time) {
	const date = new Date(time * 1000);
	date.setTime(date.getTime() + date.getTimezoneOffset()*60*1000);
	return date.getHours().toString().padStart(2, "0")
		+ ":" + date.getMinutes().toString().substr(-2).padStart(2, "0")
		+ ":" + date.getSeconds().toString().substr(-2).padStart(2, "0");
}
function stop() {
	clearInterval(intervalForTimer);
	Array.prototype.forEach.call(document.getElementsByName("stop"), x => x.style.display = "none");
	Array.prototype.forEach.call(document.getElementsByName("start"), x => x.style.display = "inline-block");
}
function continueTime(element) {
	element.childNodes[1].dataset.value++;
	element.childNodes[1].textContent = formatTime(element.childNodes[1].dataset.value);
}
function start(element) {
	stop();
	element.childNodes[2].style.display = "none";
	element.childNodes[3].style.display = "inline-block";
	intervalForTimer = setInterval(function() {continueTime(element);}, 1000);
}
document.getElementById("RednerForm").onsubmit = function(event) {
	event.preventDefault();
	// Eigenschaften von Redner
	var listElement = document.createElement("li");
	var name = document.createElement("label");
	var timer = document.createElement("label");
	var start = document.createElement("button");
	var stop = document.createElement("button");
	
	// Werte eines Redner
	name.textContent = document.getElementById("NameRedner").value;
	timer.dataset.value = 0;
	timer.textContent = formatTime(0);
	start.setAttribute("name", "start");
	start.setAttribute("onclick", "start(this.parentNode)");
	start.textContent = "Start!";
	stop.setAttribute("name", "stop");
	stop.setAttribute("onclick", "stop()");
	stop.textContent = "Stopp!";
	
	// Redner in die Liste aufnehmen / anhängen
	listElement.appendChild(name);
	listElement.appendChild(timer);
	listElement.appendChild(start);
	listElement.appendChild(stop);
	document.getElementById("rednerliste").appendChild(listElement);
	window.start(document.getElementById("rednerliste").lastElementChild);
}

