<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>5.3. Topologischer Generator</h2>
        <p>Stellen Sie bei Ihrer Klasse aus der vorletzten Aufgabe die topologische Iterierbarkeit mittels Generator her.</p>
        <p>Wählen Sie eine Implementierung, die universell gültig, also nicht nur für das Beispiel gilt.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie hier Ihren ECMAScript-Code inkl. Tests ein. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">
		
			class Vorrang { <br>
			&emsp;constructor(arr) {<br>
			&emsp; &emsp;	this.arr = arr;<br>
			&emsp;}<br>
			&emsp;topsort() {<br>
			&emsp;	&emsp;   var ordnung = [];<br>
			&emsp;	&emsp;   var poscurrent = 0;<br>
			&emsp;	&emsp;   var posnext = 0;<br>
			&emsp;	&emsp;   //Bei leere Relation<br>
			&emsp;	&emsp;   if(this.arr[0][0] === "") {<br>
			&emsp;	&emsp;   &emsp; return;<br>
			&emsp;	&emsp;   }<br>
			&emsp;	&emsp;   // Alle Tasks in der Relation werden einmalig in einer Liste aufgenommen<br>
			&emsp;	&emsp;   for(let x=0;x<this.arr.length;x++) {<br>
			&emsp;	&emsp;   	&emsp; var xpostask = this.arr[x][0];<br>
			&emsp;	&emsp;   	&emsp; var ypostask = this.arr[x][1];<br>
			&emsp;	&emsp;   	&emsp; if(!ordnung.includes(xpostask)) {<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  ordnung.push(xpostask);<br>
			&emsp;	&emsp;   	&emsp; }<br>
			&emsp;	&emsp;   	&emsp; if(!ordnung.includes(ypostask)) {<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  ordnung.push(ypostask);<br>
			&emsp;	&emsp;   	&emsp; }<br>
			&emsp;	&emsp;   }<br>
			&emsp;	&emsp;   //Nun Sortiere die Liste nach Abhängigkeiten der Relation<br>
			&emsp;	&emsp;   var fertig = false;<br>
			&emsp;	&emsp;   var tasktmp;<br>
			&emsp;	&emsp;   var countIterations = 0;<br>
			&emsp;	&emsp;   while (!fertig && countIterations < this.arr.length) {<br>
			&emsp;	&emsp;   	&emsp; fertig = true;<br>
			&emsp;	&emsp;   	&emsp; countIterations++;<br>
			&emsp;	&emsp;   	&emsp; for(let x=0;x<this.arr.length;x++) {<br>     
			&emsp;	&emsp;   	&emsp; 	&emsp;  var xpostask = this.arr[x][0];<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  var ypostask = this.arr[x][1];<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  poscurrent = ordnung.indexOf(xpostask);<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  posnext = ordnung.indexOf(ypostask);<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  if(poscurrent > posnext) {<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  	&emsp; //swap task<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  	&emsp; tasktmp = ordnung[poscurrent];<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  	&emsp; ordnung[poscurrent] = ypostask;<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  	&emsp; ordnung[posnext] = tasktmp;<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  	&emsp; fertig = false;<br>
			&emsp;	&emsp;   	&emsp; 	&emsp;  }  <br>  
			&emsp;	&emsp;   	&emsp; }  <br>  
			&emsp;	&emsp;   }<br>
			&emsp;	&emsp;   if(countIterations == this.arr.length) {<br>
			&emsp;	&emsp;   &emsp;	ordnung = new Array();<br>
			&emsp;	&emsp;   }<br>
			&emsp;	&emsp;   return ordnung;<br>
			&emsp;}<mark> <br>
			&emsp;//5.3. Topologischer Generator <br>
			&emsp;*[Symbol.iterator]() {<br>
			&emsp; &emsp;	yield* this.topsort();<br>
			&emsp;}</mark><br>
			}<br>
            const studentenLeben = new Vorrang([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]);<br>
			for ( const next of studentenLeben ){<br>
               &emsp; console.log( next );<br>
				&emsp; document.write(" " +next +" ");<br>
            }<br>
			// Testen Positv	<br>
			var expected = ["schlafen", "essen", "studieren", "prüfen"];<br>
			let i = 0;<br>
			for ( const next of studentenLeben ){<br>
			&emsp;	console.assert( expected[i++] === next, {expect: expected[i], actual:next, errorMsg: "Fehler: Werte sind ungleich -> Erwartung nicht erfuellt"});<br>	
			}<br>
			// Testen Negativ<br>
			var expected2 = ["schlafen", "essen", "prüfen", "studieren"];<br>
			i=0;<br>
			for ( const next of studentenLeben ){<br>
			&emsp;	console.assert( expected2[i++] === next, {expect: expected2[i], actual:next, errorMsg: "Fehler: Werte sind ungleich -> Erwartung nicht erfuellt"});	<br>
			}<br>
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>