<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>4.2. Objekte</h2>
        <p>Schreiben Sie die Prototypen Person und Auto in JavaScript, so dass jede Person weiß, welche Autos sie besitzt. Schreiben Sie eine Funktion conflict(), die feststellt, ob ein Auto von mehr als einer Person besessen wird.</p>
        <p>Geben Sie hier Ihren JavaScript-Code inkl. Tests ein.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Schreiben Sie die Prototypen Person und Auto in JavaScript, so dass jede Person weiß, welche Autos sie besitzt. Schreiben Sie eine Funktion conflict(), die feststellt, ob ein Auto von mehr als einer Person besessen wird.<br><br>Geben Sie hier Ihren JavaScript-Code inkl. Tests ein.</p>
        <p class="TextBlock">
		<code>
		class Auto {<br>
		&emsp;	constructor(marke, modell, getriebeArt, kennzeichen){<br>
		&emsp;&emsp;		this.marke = marke; <br>
		&emsp;&emsp;		this.modell = modell; <br>
		&emsp;&emsp;		this.getriebeArt = getriebeArt; <br>
		&emsp;&emsp;		this.kennzeichen = kennzeichen;<br>
		&emsp;	}<br>
		}<br>
		<br>
	    class Person {<br>
		&emsp;	constructor(vorname, nachname, alter, augenFarbe, auto){<br>
		&emsp;&emsp;		this.vorname = vorname; <br>
		&emsp;&emsp;		this.nachname = nachname; <br>
		&emsp;&emsp;		this.alter = alter; <br>
		&emsp;&emsp;		this.augenFarbe = augenFarbe;<br>
		&emsp;&emsp;		this.auto = auto;<br>
		&emsp;	}<br>
		&emsp;	vollerName() {<br>
		&emsp;&emsp;		return this.vorname + " " + this.nachname;<br>
		&emsp;	}<br>
		}<br>
		
		function conflict(p1, p2){<br>
		&emsp;	return (p1.auto.kennzeichen === p2.auto.kennzeichen);<br>
		}<br>
		<br>
		// Testen <br>
		var auto1 = new Auto("BMW", "M850", "Automatik", "N53117N");<br>
		var auto2 = new Auto("BMW", "M850", "Automatik", "M67149N");<br>
		var naqib = new Person("Naqib", "Niazmand", 29, "braun", auto1);<br>
		var mark = new Person("Mark", "Schu", 28, "braun", auto1);<br>
		var maik = new Person("Maik", "Nickel", 26, "braun", auto2);<br>
		var test = conflict(naqib,mark);<br>
		var test2 = conflict(naqib,maik);<br>
		document.write("Es besteht ein Konflikt: " + test +" Personen:<br> "+ naqib.vollerName() + ", "+mark.vollerName()+"<br>");<br>
		document.write("Es besteht ein Konflikt: " + test2 +" Personen:<br> "+ naqib.vollerName() + ", "+maik.vollerName());<br>
		</code>
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>