<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>10.1. Registrierung mit PHP</h2>
        
		<div class="panel-heading"><p><span>Erstellen Sie mit PHP 5 auf www2.inf.h-brs.de ein Registrierungsformular. Speichern Sie die eingegebenen Daten persistent in einer Datei auf www2.inf.h-brs.de. Schreiben Sie Ihre PHP-Scripte so, dass es zu keinen </span><a target="_blank" href="https://de.wikipedia.org/wiki/Wettlaufsituation" rel="noopener">Nebenläufigkeitsartefakten</a><span> (z.B. </span><a target="_blank" href="https://de.wikipedia.org/wiki/Verlorenes_Update" rel="noopener">Lost Update</a><span>) kommen kann, egal wie viele Nutzer sich gleichzeitig registrieren. </span></p></div>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
      	<style>
		.solution {
			font-family: Arial, Helvetica, sans-serif;
			text-align: center;
		}	
		h1{
			text-align: center;
		}	
		.einloggenDiv a {
			background-color: #FF8000;
			color: black;
			display: block;
			border: solid black;
			text-decoration: none;
			padding: 16px 16px 16px 64px;
			box-shadow: 6px 6px 6px 0 #202020;
			border: thin solid black;
			border-radius: 2rem;
			text-align: center;
		}
		.einloggenDiv {
			display:block
			text-align: center;
		}
		.einloggenDiv a:hover {
			background-color: #4CAF50;
		}
	</style>
		<h1>Sie haben Sich Erfolgreich registriert.
		<br> Klicken Sie auf den folgenden Button um sich einzuloggen</h1>
		<div class="einloggenDiv" style="margin: 0 auto; text-align: center">
			<a href="Uebung10_A10_2.php"> einloggen</a>
		</div>
    </div>
<?php include ("./includes/footer.php"); ?>
		

	