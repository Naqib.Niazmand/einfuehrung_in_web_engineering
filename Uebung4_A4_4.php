<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
<style>
// css code def. aus https://kaul.inf.h-brs.de/we/#app-content-5-2&01_BI_Orga=page-13&01_intro=page-54
// damit man die einheitlichkeit von der Aufgabenstellung nicht verloren geht. 
code {
    padding: 2px 4px;
    font-size: 90%;
    color: #c7254e;
    background-color: #f9f2f4;
    border-radius: 4px;
}
</style>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>4.4. Topsort</h2>
        <p>In jedem Projekt fallen Aufgaben (Tasks) an. Zwischen den Aufgaben gibt es paarweise Abhängigkeiten. Z.B. kann Task2 von Task1 abhängen, d.h. erst muss Task1 fertig sein, bevor Task2 starten kann, weil es dessen Ergebnisse benötigt. Tasks können auch unabhängig voneinander sein und parallel ablaufen. In JavaScript können Sie die Abhängigkeiten in Arrays codieren, z.B. kann man bei <code>[ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]</code> nicht prüfen, ohne vorher gegessen zu haben. Transitive Abhängigkeiten müssen also berücksichtigt werden.</p>
		
		<p>Schreiben Sie in JavaScript eine Funktion <code>topsort()</code>, die eine <a target="_blank" href="https://de.wikipedia.org/wiki/Topologische_Sortierung" rel="noopener">topologische Sortierung</a> berechnet.</p>
		
		<p>Achten Sie auf Performanz. Berechnen Sie die topologische Sortierung in <a target="_blank" href="https://de.wikipedia.org/wiki/Topologische_Sortierung#Gesamtverhalten" rel="noopener">linearer Zeit</a> (Average Case).</p>
		
		<p>Testen Sie Ihren JavaScript-Code. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Schreiben Sie in JavaScript eine Funktion <code>topsort()</code>, die eine <a target="_blank" href="https://de.wikipedia.org/wiki/Topologische_Sortierung" rel="noopener">topologische Sortierung</a> berechnet.<br><br>
		Achten Sie auf Performanz. Berechnen Sie die topologische Sortierung in <a target="_blank" href="https://de.wikipedia.org/wiki/Topologische_Sortierung#Gesamtverhalten" rel="noopener">linearer Zeit</a> (Average Case).
		</p>
        <p class="TextBlock">
		function topsort(arr) {<br>
	&emsp;	var ordnung = [];<br>
	&emsp;	var poscurrent = 0;<br>
	&emsp;	var posnext = 0;<br>
	&emsp;	//Bei leere Relation<br>
	&emsp;	if(arr[0][0] === "") {<br>
	&emsp;&emsp;	return;<br>
	&emsp;	}<br>
	&emsp;//Alle Tasks in der Relation werden einmalig in einer Liste aufgenommen<br>
	&emsp;for(let x=0; x < arr.length; x++) {<br>
	&emsp;&emsp;var xpostask = arr[x][0];<br>
	&emsp;&emsp;var ypostask = arr[x][1];<br>
	&emsp;&emsp;if(!ordnung.includes(xpostask)) {<br>
	&emsp;&emsp;&emsp;ordnung.push(xpostask);<br>
	&emsp;&emsp;}<br>
	&emsp;&emsp;if(!ordnung.includes(ypostask)) {<br>
	&emsp;&emsp;&emsp;ordnung.push(ypostask);<br>
	&emsp;&emsp;}<br>
	&emsp;}<br>
	&emsp;//Nun Sortiere die Liste nach Abhängigkeiten der Relation<br>
	&emsp;  var fertig = false;<br>
	&emsp;	var tasktmp;<br>
	&emsp;	var countIterations = 0;<br>
	&emsp;	while (!fertig && countIterations < arr.length) {<br>
	&emsp;&emsp;fertig = true;<br>
	&emsp;&emsp;countIterations++;<br>
	&emsp;for(let x=0; x < arr.length; x++) { <br>    
	&emsp;&emsp;&emsp;var xpostask = arr[x][0];<br>
	&emsp;&emsp;&emsp;var ypostask = arr[x][1];<br>
	&emsp;&emsp;&emsp;poscurrent = ordnung.indexOf(xpostask);<br>
	&emsp;&emsp;&emsp;posnext = ordnung.indexOf(ypostask);<br>
	&emsp;&emsp;&emsp;if(poscurrent > posnext) {<br>
	&emsp;&emsp;&emsp;&emsp;//swap task<br>
	&emsp;&emsp;&emsp;&emsp;tasktmp = ordnung[poscurrent];<br>
	&emsp;&emsp;&emsp;&emsp;ordnung[poscurrent] = ypostask;<br>
	&emsp;&emsp;&emsp;&emsp;ordnung[posnext] = tasktmp;<br>
	&emsp;&emsp;&emsp;&emsp;fertig = false;<br>
	&emsp;&emsp;&emsp;}<br>    
	&emsp;&emsp;}<br>    
	&emsp;	}<br>
	&emsp;	if(countIterations == arr.length) {<br>
	&emsp;&emsp;ordnung = new Array();<br>
	&emsp;	}<br>
	&emsp;	return ordnung;<br>
		} <br>
		</p>
		
		<p class="description">Testen Sie Ihren JavaScript-Code. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">
		// Testen Positv<br><br>	
		var test = topsort([ ["schlafen", "studieren"], ["essen", "studieren"], ["studieren", "prüfen"] ]);<br>
		var expected = ["schlafen", "essen", "studieren", "prüfen"];<br><br>
		for(let i = 0; i < expected.length; i++){<br>
		&emsp;&emsp;console.assert( expected[i] === test[i],<br> 
		&emsp;&emsp;{errorMsg: "Fehler: Werte sind ungleich <br>
		&emsp;&emsp;-> Erwartung nicht erfuellt"});<br>	
		}<br><br><br>
		
		// Testen Negativ<br><br>
		var expected2 = ["schlafen", "essen", "prüfen", "studieren"];<br><br>
		for(let i = 0; i < expected2.length; i++){<br>
		&emsp;&emsp;console.assert( expected2[i] === test[i], <br>
		&emsp;&emsp;{expect: expected2[i], actual:test[i], <br>
		&emsp;&emsp;errorMsg: "Fehler: Werte sind ungleich -> <br>
		&emsp;&emsp;Erwartung nicht erfuellt"});<br>	
		}<br><br>
		document.write("Ergebnis: "+ test);<br>
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>