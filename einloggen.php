<?PHP
if ( isset($_POST[ 'benutzername' ]) && isset($_POST[ 'passwort' ]) ) { // zum einloggen muss beides existieren. 
    $eingegebenerBenutzername = $_POST['benutzername']; // Benutzer Daten
    $eingegebenerPasswort = $_POST['passwort'];
    $benutzern  = file('./eingegebenenDatenU10.csv'); // Speichern Sie die eingegebenen Daten persistent in einer Datei
    $eingelogt = false;  
	
	/*https://www.php.net/manual/de/control-structures.foreach.php*
		foreach (array_expression as $key => $value)
			statement
	*/
    foreach ($benutzern as $benutzerKey => $benutzer) {
        list($gespeichertesBenutzer, $gespeichertesPasswort) = explode(",", $benutzer); // benutzern
        if ( ((strcmp(trim($gespeichertesBenutzer), trim($eingegebenerBenutzername))===0) && 
				(strcmp(trim($gespeichertesPasswort), trim($eingegebenerPasswort))===0))) {
					$eingelogt = true;
					break;
        }
    }
    if( $eingelogt === true) {
        header("Location: eingelogt.php");
        exit;
    } else {
        header("Location: Uebung10_A10_2.php");
        exit;
    }
}
?>