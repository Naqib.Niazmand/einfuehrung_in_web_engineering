<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>

	<div class="task">
		<h1>Beschreibung der Aufgabe</h1>
		<h2>1.1. Fachliche Argumentation über Erfolgsprinzipien des WWW</h2>
		<p>Mit welchen fachlichen Argumenten wurde das WWW-Proposal von TBL abgelehnt?</p>
		<p>Was sind die fachlichen Argumente, warum das WWW dennoch ein Erfolg wurde?</p>
		<p>Was wäre der Preis für die garantierte Verhinderung von “broken links”?</p>
	</div>
	<div class="solution">
		<h1>Lösung der Aufgabe</h1>
		<p class="description">Mit welchen fachlichen Argumenten wurde das WWW-Proposal von TBL abgelehnt?</p>
		<p class="TextBlock">Das Proposal wurde abgelehnt, weil die Hypertext Community der Ansicht war, dass die Wissenschaft bereits fortgeschrittener sei. Insbesondere wegen der Möglichkeit von "broken links", die in dem Proposal von TBL möglich sind. Referentielle Integrität von Hyperlinks war also eine Voraussetzung der damaligen Jury. Außerdem wurde die Evolvierbarkeit (Erweiterbarkeit) als vage missverstanden. </p>
		<p class="description">Was sind die fachlichen Argumente, warum das WWW dennoch ein Erfolg wurde?</p>
		<p class="TextBlock">Das WWW hatte Erfolg, da sich die unterschiedlichen eingesetzten Technologien voneinader unabhängig weiterentwickeln können. Außerdem hatte die Dezentralität von WWW die Folge, dass der Dienst sehr frei bzw. offen nutzbar war. </p>
		<p class="description">Was wäre der Preis für die garantierte Verhinderung von “broken links”?</p>
		<p class="TextBlock">Der Preis für eine garantierte Verhinderung von "broken links" wäre eine zentrale Kontrolle des WWW mit einer Umsetzung von referentieller Integrität für Hyperlinks.</p>
	</div>
	
<?php include ("./includes/footer.php"); ?>