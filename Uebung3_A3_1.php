<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
        <div class="task">
            <h1>Beschreibung der Aufgabe</h1>
            <h2>3.1. Responsiv mit Flexbox Desktop-First</h2>
            <p>Spielen Sie zunächst das Flexbox Froggy-Spiel, um Flexbox zu lernen.<br><br>
			   Implementieren Sie dann ausschließlich mit HTML und CSS Flexbox folgendes responsive Webdesign nach der Desktop-First-Strategie!</p>
			   <img alt="Responsiv mit Flexbox Desktop-First" src="https://kaul.inf.h-brs.de/we/assets/img/holy-grail1.png" width="100%">
			   <p>Geben Sie hier HTML- und CSS-Code zusammen ein: Siehe Rechts Lösungsseite</p>		
        </div>
        <div class="solution">
            <h1>Lösung der Aufgabe</h1>
            <p class="description">Implementieren Sie dann ausschließlich mit HTML und CSS Flexbox folgendes responsive Webdesign nach der Desktop-First-Strategie!</p>
            
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
		<style>
		/*nach der Desktop-First-Strategie!*/
		body {
			margin: 0;
			padding: 0;
		}
		.containerDesktopA3 {
			display: flex;
			flex-direction: column;  
			margin: 10px;   
		}
		
		.containerTabletAndMobileA3 {
			display: flex;
			flex-direction: row;   
		}
		
		.rotHeader {   
			width: 100%; 
			background-color:rgb(255,37,0);
			height: 25vh;  
		}
		
		.gruenMenu {   
			width: 25%;
			background-color:rgb(44,238,39);
			margin-right: 10px;
			margin-top: 10px; 
			height: 75vh;
		}
		
		.blauContent {   
			width: 50%;
			background-color:rgb(5,51,255);
			margin-right: 10px;
			margin-top: 10px;
			height: 75vh;
		}
		
		.pinkAside {   
			width: 25%;   
			background-color:rgb(234,62,254);
			margin-top: 10px;  
			height: 75vh;   
		}
		
		/*Responsive Design für die Tablet Ansicht*/
		@media (max-width: 650px) {
		
			.containerTabletAndMobileA3 {
				flex-wrap: wrap;
			}    
			
			.rotHeader {  
				height: 10vh;
				width: 100%;
			}
			
			.gruenMenu {  
				width: calc(25% -20px);  
				height: 100vh;   
			}
			
			.blauContent {  
				width: calc(75% - 20px); ; 
				height: 100vh;  
			}
			
			.pinkAside { 
				width: 100%;
				height: 10vh;  
			}
		}
		
		
		/*Responsive Design für die Mobiele Ansicht*/
		@media (max-width: 400px) {
		
			.containerTabletAndMobileA3 {
				flex-wrap: wrap;
			}
			
			.rotHeader {  
				height: 10vh;
				width: 100%;
				margin: 0 0 0 0;
			}
		
			.gruenMenu {  
				width: 100%;  
				height: 10vh;  
				margin: 0 0 0 0;
			}
			
			.blauContent {  
				width: 100%; 
				height: 125vh; 
				margin: 0 0 0 0;
			}
			
			.pinkAside { 
				width: 100%;
				height: 10vh;  
				margin: 0 0 0 0;
			}
		}
		</style>
	</head>
	<body>
		<div class="containerDesktopA3">
			<div class="rotHeader">Header </div>
			<div class="containerTabletAndMobileA3">
				<div class="gruenMenu"> Menu </div> 
				<div class="blauContent"> Content</div>
				<div class="pinkAside"> Aside</div>
			</div> 
		</div>
	</body>
</html>
        </div>
<?php include ("./includes/footer.php"); ?>
