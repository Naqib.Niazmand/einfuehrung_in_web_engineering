<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 10.3: WWW-Navigator zum Content-Editor ausbauen</h2>
        <p><span>Bauen Sie Ihren WWW-Navigator zum Content-Editor aus, mit dem Sie weitere Inhalte hinzu fügen können, die persistent auf dem Server mittels PHP gespeichert werden. Schreiben Sie Ihre PHP-Scripte so, dass es zu keinen </span><a target="_blank" href="https://de.wikipedia.org/wiki/Wettlaufsituation" rel="noopener">Nebenläufigkeitsartefakten</a><span> kommen kann, egal wie viele Nutzer gleichzeitig editieren und speichern.</span></p>
		
		<p><span>Speichern Sie die Inhalte Ihres WWW-Navigators auf dem Server www2.inf.h-brs.de. Erweitern Sie Ihren WWW-Navigator um eine Edit-Funktionalität, so dass Inhalte editiert und ergänzt werden können. Vermeiden Sie die </span><a target="_blank" href="https://de.wikipedia.org/wiki/Verlorenes_Update" rel="noopener">Lost Update</a><span>-Anomalie.</span></p>
		
		<p>Schützen Sie Ihre Inhalte mit einem Login. </p>
		
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="TextBlock">Das einloggen und ausloggen habe ich in den Aufgaben 10.1 und 10.2 eingebaut.<br>
			Leider ist die Zeit knapp um die 10.3 ausführlich durchzuführen,
			daher wird diese Teilaufgabe vorerst übersprungen.<br>
			</p>
    </div>
<?php include ("./includes/footer.php"); ?>