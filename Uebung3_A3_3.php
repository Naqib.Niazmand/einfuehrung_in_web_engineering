<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>

        <div class="task">
            <h1>Beschreibung der Aufgabe</h1>
            <h2>3.3. Responsiv mit Grid</h2>
            <p>Implementieren Sie folgende Landing Page responsiv mit Grid Layout. Vermeiden Sie außerdem das Erscheinen von Scrollbars so weit wie möglich.</p>
            <img alt="Responsiv mit Grid COMPLETE SERIES JS" src="https://kaul.inf.h-brs.de/we/assets/img/landing.png" width="100%">
			<P>Für das Bild dürfen Sie die URL <a href="https://kaul.inf.h-brs.de/we/assets/img/landing-img.png">https://kaul.inf.h-brs.de/we/assets/img/landing-img.png</a> verwenden.</P>
			<p>Geben Sie hier HTML- und CSS-Code zusammen ein: Siehe Rechts Lösungsseite</p>	
        </div>
        <div class="solution">
			<h1>Lösung der Aufgabe</h1>
			<p class="description">Implementieren Sie folgende Landing Page responsiv mit Grid Layout. Vermeiden Sie außerdem das Erscheinen von Scrollbars so weit wie möglich.</p>
			
			<!DOCTYPE html>
			<html>
				<head>
					<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
					<style>
						.solutionBody {
							flex-wrap: 60%;
							margin: 0;
							font-family: arial, sans-serif;
						}
						.containerU3A3{
							box-sizing: border-box;
							display: grid;
							height: 100vh;
							width: 100%;
							grid-template-columns: 50% 50%;
							grid-template-rows: 7% 11% 62% 20%;
						}
						.headerU3A3{
							background-color: rgb(54,54,54);
							text-align: center;
							color: lightgrey;
							grid-column: 1/3;
							grid-row: 1/2;
						}
						.headerU3A3 ul li{
							padding: 0vh 1.5vw 0vh 1.5vw;
							color: white;
							font-size: 2vh;
							list-style: none;
							display: inline;
						}
						.title{
							background-color: rgb(232,231,226);
							grid-column: 1/3;
							grid-row: 2/3;
							text-align: center;
							font-weight: bold;
							font-size: 3.3vh;
							font-family: 'Arimo', sans-serif;
						}
						.PicContent{
							background-color: rgb(232,231,226);
							grid-column: 1/2;
							grid-row: 3/4;
						}
						#imgPicContent{
							width: 70%;
							height: 93%;
							border: solid black 1px;
							border-radius: 7px;
							display: block;
							margin: auto;
						}
						.textContent{
							background-color: rgb(232,231,226);
							grid-column: 2/3; 
							grid-row: 3/4;
							text-align: center;
							font-size: 2.8vh;
						}
						.textContent > p{
							padding: 3vh 0vw 0vh 0vw;
						}
						
						.containerU3A3 button{
							background-color: rgb(246, 142, 66);
							border-radius: 4px;
							border: none;
							color: white;
							font-family: 'Fjalla One', sans-serif;
							font-size: 1.8vh;
						}
						
						.buttonContent{
							margin: 3vh 0vw;
							padding: 1vh 3vw;
						}
						.footerU3A3{
							background-color: rgb(0,24,33);
							grid-column: 1/3;
							grid-row: 4/5;
							text-align: center;
							font-size: 2vh;
							color: lightgrey;
							padding-top: 2vh;
						}
						.buttonFooter{
							padding: 1vh 5vw;
						}
				
						/*Responsive Design für Tablet Ansicht*/
						@media screen and (max-width: 600px) {
							.containerU3A3{
								height: 120vh;
								width: 100%;
								grid-template-columns: 100%;
								grid-template-rows: 7% 11% 31% 31% 20%;
							}
							.headerU3A3{
								grid-column: 1/2;
								grid-row: 1/2;
							}
							.title{
								grid-column: 1/2;
								grid-row: 2/3;
							}
							.PicContent{
								grid-column: 1/2;
								grid-row: 3/4;
							}
							.textContent{
								grid-column: 1/2;
								grid-row: 4/5;
							}
							.footerU3A3{
								grid-column: 1/2;
								grid-row: 5/6;
							}
						}
				
						/*Responsive Design für Mobile Ansicht*/
						@media screen and (max-width: 300px) {
							.containerU3A3{
								height: 140vh;
								width: 100%;
								grid-template-columns: 100%;
								grid-template-rows: 11% 12% 31.5% 31.5% 15%;
							}
							.headerU3A3{
								grid-column: 1/2;
								grid-row: 1/2;
							}
							.headerU3A3 ul li{
								display: block;
							}
							.title{
								grid-column: 1/2;
								grid-row: 2/3;
							}
							.PicContent{
								grid-column: 1/2;
								grid-row: 3/4;
							}
							.textContent{
								grid-column: 1/2;
								grid-row: 4/5;
							}
							.footerU3A3{
								grid-column: 1/2;
								grid-row: 5/6;	
							}
						}
					</style>
				</head>
				<body>
				<div class="solutionBody">
					<div class="containerU3A3">
						<div class="headerU3A3">
							<ul>
								<li>The book series</li>
								<li>Testimonials</li>
								<li>The Autor</li>
								<li>Free resources</li>
							</ul>
						</div>
						<div class="title">
							<p>You &nbsp; dont &nbsp; know &nbsp; JavaScript</p>
						</div>
						<div class="PicContent">
							<img src="https://kaul.inf.h-brs.de/we/assets/img/landing-img.png" id="imgPicContent">
						</div>
						<div class="textContent">
							<p>Don't just drift through Javascript.</p>
							<p>Understand how Javascript works</p>
							<p>Start your journey through the bumpy side of Javascript</p>
							<button class="buttonContent">ORDER YOUR COPY NOW</button>
						</div>
						<div class="footerU3A3">
							<br>
							<p>The first ebook in the book seriesis absolutely free.</p>
							<button class="buttonFooter">FIND OUT MORE ABOUT THIS OFFER</button>
						</div>
					</div>
				</div>	
				</body>
			</html>
        </div>

<?php include ("./includes/footer.php"); ?>
