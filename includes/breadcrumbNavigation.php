<?php 
	$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
	$file = $crumbs[sizeof($crumbs)-1];
	$filename = strval($file);
	if(strcmp($filename, 'index.php') == 0 || (strcmp($filename, 'index') == 0) || (strcmp($filename, '') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung1_A1_1.php') == 0) || (strcmp($filename, 'Uebung1_A1_2.php') == 0)|| (strcmp($filename, 'Uebung1_A1_3.php') == 0)|| (strcmp($filename, 'Uebung1_A1_4.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung1_A1_1.php">Übung 1 HTML</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung2_A2_1.php') == 0)||(strcmp($filename, 'Uebung2_A2_2.php') == 0)||(strcmp($filename, 'Uebung2_A2_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung2_A2_1.php">Übung 2 CSS-Teil-1</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung3_A3_1.php') == 0)||(strcmp($filename, 'Uebung3_A3_2.php') == 0)||(strcmp($filename, 'Uebung3_A3_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung3_A3_1.php">Übung 3 CSS-Teil-2</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung4_A4_1.php') == 0)||(strcmp($filename, 'Uebung4_A4_2.php') == 0)||(strcmp($filename, 'Uebung4_A4_3.php') == 0)||(strcmp($filename, 'Uebung4_A4_4.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung4_A4_1.php">Übung 4 JavaScript</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung5_A5_1.php') == 0)||(strcmp($filename, 'Uebung5_A5_2.php') == 0)||(strcmp($filename, 'Uebung5_A5_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung5_A5_1.php">Übung 5 ECMAScript</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung6_A6_1.php') == 0)||(strcmp($filename, 'Uebung6_A6_2.php') == 0)||(strcmp($filename, 'Uebung6_A6_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung6_A6_1.php">Übung 6 Functional</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung7_A7_1.php') == 0)||(strcmp($filename, 'Uebung7_A7_2.php') == 0)||(strcmp($filename, 'Uebung7_A7_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung7_A7_1.php">Übung 7 DOM</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung8_A8_1.php') == 0)||(strcmp($filename, 'Uebung8_A8_2.php') == 0)||(strcmp($filename, 'Uebung8_A8_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung8_A8_1.php">Übung 8 Async</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung9_A9_1.php') == 0)||(strcmp($filename, 'Uebung9_A9_2.php') == 0)||(strcmp($filename, 'Uebung9_A9_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung9_A9_1.php">Übung 9 Vue</a>
			 </div>';
	}else if((strcmp($filename, 'Uebung10_A10_1.php') == 0)||(strcmp($filename, 'Uebung10_A10_2.php') == 0)||(strcmp($filename, 'Uebung10_A10_3.php') == 0)){
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="Uebung10_A10_1.php">Übung 10 PHP</a>
			 </div>';
	}else{
		echo '<div class="breadcrumbNavigation">
				<a href="index.php">Übung auswählen</a>
				<p id="arrow">--></p>
				<a href="#">Fehler</a>
			 </div>';
	}
?>
