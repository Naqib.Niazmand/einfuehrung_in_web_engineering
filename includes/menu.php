<?php 
	echo '<div class="containerTabletAndMobile">';
// eine Toast Message, dass eine Übung und dazugehörige Aufgabe Ausgewählt werden soll.
echo'
<style>
	#toastMessage {
	visibility: hidden;
	min-width: 250px;
	margin-left: -125px;
	background-color: #333;
	color: #fff;
	text-align: center;
	border-radius: 2px;
	padding: 16px;
	position: fixed;
	z-index: 1;
	left: 50%;
	top: 30px;
	font-size: 17px;
	}
	
	#toastMessage.show {
	visibility: visible;
	-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
	animation: fadein 0.5s, fadeout 0.5s 10.5s;
	}
	
	#AufgabeAuswaehlen {
		background-color: #eee;
		border: 3px solid red;
		color: black;
		padding: 16px 64px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		font-weight: bold;
	}
	
	#AufgabeAuswaehlen:hover {
		background-color: #4CAF50;
	}

	@-webkit-keyframes fadein {
	from {top: 0; opacity: 0;} 
	to {top: 30px; opacity: 1;}
	}
	
	@keyframes fadein {
	from {top: 0; opacity: 0;}
	to {top: 30px; opacity: 1;}
	}
	
	@-webkit-keyframes fadeout {
	from {top: 30px; opacity: 1;} 
	to {top: 0; opacity: 0;}
	}
	
	@keyframes fadeout {
	from {top: 30px; opacity: 1;}
	to {top: 0; opacity: 0;}
	}
</style>
	<div id="toastMessage">Bitte Wählen Sie eine Übung und die dazugehörige Aufgabe aus!</div>
<script>
	function showToastMessage() {
		var x = document.getElementById("toastMessage");
		x.className = "show";
		setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
	} 
</script>';

	$crumbs = explode("/",$_SERVER["REQUEST_URI"]);
	$file = $crumbs[sizeof($crumbs)-1];
	$filename = strval($file);
	if(strcmp($filename, 'index.php') == 0 || (strcmp($filename, 'index') == 0) || (strcmp($filename, '') == 0)){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Übung auswählen</button></li>
					<li><a href="Uebung1_A1_1.php">Übung 1 HTML</a></li>
					<li><a href="Uebung2_A2_1.php">Übung 2 CSS-Teil-1</a></li>
					<li><a href="Uebung3_A3_1.php">Übung 3 CSS-Teil-2</a></li>
					<li><a href="Uebung4_A4_1.php">Übung 4 JavaScript</a></li>
					<li><a href="Uebung5_A5_1.php">Übung 5 ECMAScript</a></li>
					<li><a href="Uebung6_A6_1.php">Übung 6 Functional</a></li>
					<li><a href="Uebung7_A7_1.php">Übung 7 DOM</a></li>
					<li><a href="Uebung8_A8_1.php">Übung 8 Async</a></li>
					<li><a href="Uebung9_A9_1.php">Übung 9 Vue</a></li>
					<li><a href="Uebung10_A10_1.php">Übung 10 PHP</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung1_A1_1.php') == 0){
		echo '
		<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung1_A1_1.php" class="active" >Aufgabe 1.1</a></li>
					<li><a href="Uebung1_A1_2.php" >Aufgabe 1.2</a></li>
					<li><a href="Uebung1_A1_3.php" >Aufgabe 1.3</a></li>
					<li><a href="Uebung1_A1_4.php" >Aufgabe 1.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung1_A1_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung1_A1_1.php" >Aufgabe 1.1</a></li>
					<li><a href="Uebung1_A1_2.php" class="active" >Aufgabe 1.2</a></li>
					<li><a href="Uebung1_A1_3.php" >Aufgabe 1.3</a></li>
					<li><a href="Uebung1_A1_4.php" >Aufgabe 1.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung1_A1_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung1_A1_1.php" >Aufgabe 1.1</a></li>
					<li><a href="Uebung1_A1_2.php" >Aufgabe 1.2</a></li>
					<li><a href="Uebung1_A1_3.php" class="active" >Aufgabe 1.3</a></li>
					<li><a href="Uebung1_A1_4.php" >Aufgabe 1.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung1_A1_4.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung1_A1_1.php" >Aufgabe 1.1</a></li>
					<li><a href="Uebung1_A1_2.php" >Aufgabe 1.2</a></li>
					<li><a href="Uebung1_A1_3.php" >Aufgabe 1.3</a></li>
					<li><a href="Uebung1_A1_4.php" class="active" >Aufgabe 1.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung2_A2_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung2_A2_1.php" class="active">Aufgabe 2.1</a></li>
					<li><a href="Uebung2_A2_2.php" >Aufgabe 2.2</a></li>
					<li><a href="Uebung2_A2_3.php" >Aufgabe 2.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung2_A2_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung2_A2_1.php">Aufgabe 2.1</a></li>
					<li><a href="Uebung2_A2_2.php" class="active">Aufgabe 2.2</a></li>
					<li><a href="Uebung2_A2_3.php">Aufgabe 2.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung2_A2_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung2_A2_1.php">Aufgabe 2.1</a></li>
					<li><a href="Uebung2_A2_2.php">Aufgabe 2.2</a></li>
					<li><a href="Uebung2_A2_3.php" class="active">Aufgabe 2.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung3_A3_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung3_A3_1.php" class="active">Aufgabe 3.1</a></li>
					<li><a href="Uebung3_A3_2.php">Aufgabe 3.2</a></li>
					<li><a href="Uebung3_A3_3.php">Aufgabe 3.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung3_A3_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung3_A3_1.php">Aufgabe 3.1</a></li>
					<li><a href="Uebung3_A3_2.php" class="active">Aufgabe 3.2</a></li>
					<li><a href="Uebung3_A3_3.php" >Aufgabe 3.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung3_A3_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung3_A3_1.php">Aufgabe 3.1</a></li>
					<li><a href="Uebung3_A3_2.php">Aufgabe 3.2</a></li>
					<li><a href="Uebung3_A3_3.php" class="active">Aufgabe 3.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung4_A4_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung4_A4_1.php" class="active">Aufgabe 4.1</a></li>
					<li><a href="Uebung4_A4_2.php">Aufgabe 4.2</a></li>
					<li><a href="Uebung4_A4_3.php">Aufgabe 4.3</a></li>
					<li><a href="Uebung4_A4_4.php">Aufgabe 4.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung4_A4_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung4_A4_1.php">Aufgabe 4.1</a></li>
					<li><a href="Uebung4_A4_2.php" class="active">Aufgabe 4.2</a></li>
					<li><a href="Uebung4_A4_3.php">Aufgabe 4.3</a></li>
					<li><a href="Uebung4_A4_4.php">Aufgabe 4.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung4_A4_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung4_A4_1.php">Aufgabe 4.1</a></li>
					<li><a href="Uebung4_A4_2.php">Aufgabe 4.2</a></li>
					<li><a href="Uebung4_A4_3.php" class="active">Aufgabe 4.3</a></li>
					<li><a href="Uebung4_A4_4.php">Aufgabe 4.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung4_A4_4.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung4_A4_1.php">Aufgabe 4.1</a></li>
					<li><a href="Uebung4_A4_2.php">Aufgabe 4.2</a></li>
					<li><a href="Uebung4_A4_3.php">Aufgabe 4.3</a></li>
					<li><a href="Uebung4_A4_4.php" class="active">Aufgabe 4.4</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung5_A5_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung5_A5_1.php" class="active">Aufgabe 5.1</a></li>
					<li><a href="Uebung5_A5_2.php">Aufgabe 5.2</a></li>
					<li><a href="Uebung5_A5_3.php">Aufgabe 5.3</a></li>
					<li><a href="Uebung5_A5_4.php">Aufgabe 5.4</a></li>
					<li><a href="Uebung5_A5_5.php">Aufgabe 5.5</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung5_A5_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung5_A5_1.php">Aufgabe 5.1</a></li>
					<li><a href="Uebung5_A5_2.php" class="active">Aufgabe 5.2</a></li>
					<li><a href="Uebung5_A5_3.php">Aufgabe 5.3</a></li>
					<li><a href="Uebung5_A5_4.php">Aufgabe 5.4</a></li>
					<li><a href="Uebung5_A5_5.php">Aufgabe 5.5</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung5_A5_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung5_A5_1.php">Aufgabe 5.1</a></li>
					<li><a href="Uebung5_A5_2.php">Aufgabe 5.2</a></li>
					<li><a href="Uebung5_A5_3.php" class="active">Aufgabe 5.3</a></li>
					<li><a href="Uebung5_A5_4.php">Aufgabe 5.4</a></li>
					<li><a href="Uebung5_A5_5.php">Aufgabe 5.5</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung5_A5_4.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung5_A5_1.php">Aufgabe 5.1</a></li>
					<li><a href="Uebung5_A5_2.php">Aufgabe 5.2</a></li>
					<li><a href="Uebung5_A5_3.php">Aufgabe 5.3</a></li>
					<li><a href="Uebung5_A5_4.php" class="active">Aufgabe 5.4</a></li>
					<li><a href="Uebung5_A5_5.php">Aufgabe 5.5</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung5_A5_5.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung5_A5_1.php">Aufgabe 5.1</a></li>
					<li><a href="Uebung5_A5_2.php">Aufgabe 5.2</a></li>
					<li><a href="Uebung5_A5_3.php">Aufgabe 5.3</a></li>
					<li><a href="Uebung5_A5_4.php">Aufgabe 5.4</a></li>
					<li><a href="Uebung5_A5_5.php" class="active">Aufgabe 5.5</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung6_A6_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung6_A6_1.php" class="active">Aufgabe 6.1</a></li>
					<li><a href="Uebung6_A6_2.php">Aufgabe 6.2</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung6_A6_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung6_A6_1.php">Aufgabe 6.1</a></li>
					<li><a href="Uebung6_A6_2.php" class="active">Aufgabe 6.2</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung7_A7_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung7_A7_1.php" class="active">Aufgabe 7.1</a></li>
					<li><a href="Uebung7_A7_2.php">Aufgabe 7.2</a></li>
					<li><a href="Uebung7_A7_3.php">Aufgabe 7.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung7_A7_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung7_A7_1.php">Aufgabe 7.1</a></li>
					<li><a href="Uebung7_A7_2.php" class="active">Aufgabe 7.2</a></li>
					<li><a href="Uebung7_A7_3.php">Aufgabe 7.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung7_A7_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung7_A7_1.php">Aufgabe 7.1</a></li>
					<li><a href="Uebung7_A7_2.php">Aufgabe 7.2</a></li>
					<li><a href="Uebung7_A7_3.php" class="active">Aufgabe 7.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung8_A8_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung8_A8_1.php" class="active">Aufgabe 8.1</a></li>
					<li><a href="Uebung8_A8_2.php">Aufgabe 8.2</a></li>
					<li><a href="Uebung8_A8_3.php">Aufgabe 8.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung8_A8_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung8_A8_1.php">Aufgabe 8.1</a></li>
					<li><a href="Uebung8_A8_2.php" class="active">Aufgabe 8.2</a></li>
					<li><a href="Uebung8_A8_3.php">Aufgabe 8.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung8_A8_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung8_A8_1.php">Aufgabe 8.1</a></li>
					<li><a href="Uebung8_A8_2.php" >Aufgabe 8.2</a></li>
					<li><a href="Uebung8_A8_3.php" class="active">Aufgabe 8.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung9_A9_1.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung9_A9_1.php" class="active">Aufgabe 9.1</a></li>
					<li><a href="Uebung9_A9_2.php">Aufgabe 9.2</a></li>
					<li><a href="Uebung9_A9_3.php">Aufgabe 9.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung9_A9_2.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung9_A9_1.php">Aufgabe 9.1</a></li>
					<li><a href="Uebung9_A9_2.php" class="active">Aufgabe 9.2</a></li>
					<li><a href="Uebung9_A9_3.php">Aufgabe 9.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung9_A9_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung9_A9_1.php">Aufgabe 9.1</a></li>
					<li><a href="Uebung9_A9_2.php">Aufgabe 9.2</a></li>
					<li><a href="Uebung9_A9_3.php" class="active">Aufgabe 9.3</a></li>
				</ul>
			</div>';
	}else if((strcmp($filename, 'Uebung10_A10_1.php') == 0) || (strcmp($filename, 'registrierungErfolgreich.php') == 0)){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung10_A10_1.php" class="active">Aufgabe 10.1</a></li>
					<li><a href="Uebung10_A10_2.php">Aufgabe 10.2</a></li>
					<li><a href="Uebung10_A10_3.php">Aufgabe 10.3</a></li>
				</ul>
			</div>';
	}else if((strcmp($filename, 'Uebung10_A10_2.php') == 0) || (strcmp($filename, 'eingelogt.php') == 0)){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung10_A10_1.php">Aufgabe 10.1</a></li>
					<li><a href="Uebung10_A10_2.php" class="active">Aufgabe 10.2</a></li>
					<li><a href="Uebung10_A10_3.php">Aufgabe 10.3</a></li>
				</ul>
			</div>';
	}else if(strcmp($filename, 'Uebung10_A10_3.php') == 0){
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="Uebung10_A10_1.php">Aufgabe 10.1</a></li>
					<li><a href="Uebung10_A10_2.php">Aufgabe 10.2</a></li>
					<li><a href="Uebung10_A10_3.php" class="active">Aufgabe 10.3</a></li>
				</ul>
			</div>';
	}else{
		echo '<div class="menu">
				<ul>
					<li><button onclick="showToastMessage()" id="AufgabeAuswaehlen">Aufgabe auswählen</button></li>
					<li><a href="#">Aufgabe X</a></li>
					<li><a href="#">Aufgabe Y</a></li>
					<li><a href="#">Aufgabe Z</a></li>
				</ul>
			</div>';
	}
?>