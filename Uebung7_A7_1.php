<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
		<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			background-color: #f9f2f4;
			border-radius: 4px;;
		}
		</style>
        <h1>Beschreibung der Aufgabe</h1>
        <h2>7.1. Performanzmessungen von DOM-Operationen</h2>
		 	
        <div class="panel-heading"><p><span>Implementieren Sie Performanzmessungen zum Vergleich von </span><code>innerHTML, innerText, textContent und outerHTML</code><span> selbstständig in JavaScript durch Nutzung der </span><a target="_blank" href="https://www.w3schools.com/js/js_htmldom.asp" rel="noopener">DOM API</a><span>. Geben Sie die Messergebnisse als Tabelle aus. Verwenden Sie die eingebauten Zeitmess-Funktionen </span><a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/API/Performance/now" rel="noopener">performance.now ()</a><span>, siehe auch </span><a target="_blank" href="https://developers.google.com/web/updates/2012/08/When-milliseconds-are-not-enough-performance-now" rel="noopener">When-milliseconds-are-not-enough-performance-now</a><span>. Suchen Sie eine möglichst kurze und elegante Lösung.</span></p><p><span>Dabei ist zu beachten, dass Browser, um potenzielle Sicherheitsbedrohungen wie </span><a target="_blank" href="https://spectreattack.com/" rel="noopener">Spectre</a><span> oder </span><a target="_blank" href="https://de.wikipedia.org/wiki/Meltdown_(Sicherheitsl%C3%BCcke)" rel="noopener">Meltdown</a><span> zu minimieren, den zurückgegebenen Wert normalerweise um einen bestimmten Betrag runden. Dies führt zu einer gewissen Ungenauigkeit. Beispielsweise rundet Firefox die zurückgegebene Zeit in Schritten von 1 Millisekunde. Diese Zwangsrundung kann man jedoch wiederum abschalten mittels Firefox setting </span><code>privacy.reduceTimerPrecision</code><span>, siehe </span><a target="_blank" href="https://stackoverflow.com/questions/50117537/how-to-get-microsecond-timings-in-javascript-since-spectre-and-meltdown" rel="noopener">How to get microsecond timings in JavaScript since Spectre and Meltdown</a><span>.</span></p></div>
        
    </div>
    <div class="solution" id="U7A7_1">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">7.1. Performanzmessungen von DOM-Operationen</p>

		
        <style>
            #messungenTabId {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
				text-align: center;
            }

            #messungenTabId td, #messungenTabId th {
                border: 1px solid #E0E0E0;
                padding: 8px;
				text-align: center;
            }

            #messungenTabId tr:nth-child(even){background-color: #E0E0E0;}

            #messungenTabId tr:hover {background-color: #FF8000;}

            #messungenTabId th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: center;
                background-color: #FF8000;
                color: white;
            }
        </style>
        <script>
		
			/*Dozenten Feedback:
				Es gehört zu den Grundlagen der Messtechnik, dass man die Genauigkeit und Zuverlässigkeit der Messungen dadurch erhöht, dass man die Messungen mehrfach durchführt.Es reicht also nicht aus, dass Sie nur einen Durchlauf messen.
				
				Daher die alte Lösung optimieren / korrigieren. 
				*/
            const durchlaeufAnzahl = 30;
			// Messungen in 2D Array für die Tabelle abspeichern.
			var messungen = new Array(durchlaeufAnzahl); 
			//2D array mit 1D Array Erzeugen
			for (var i = 0; i < messungen.length; i++) { 
				messungen[i] = new Array(4); // 4 Spalten
			}
			
			// Messungen
            for( let durchlauf = 0; durchlauf < durchlaeufAnzahl; durchlauf++) {

				obj1 = document.createElement('div')
				document.getElementById("U7A7_1").appendChild(obj1);
				//Zeitmessung von innerHTML
				const t0 = performance.now();
				obj1.innerHTML = " Das ist ein Test ";
				const t1 = performance.now();
				let t_innerHTML = (t1 - t0);
				console.log(`Call to innerHTML took ${t_innerHTML} milliseconds.`);
				messungen[durchlauf][0] = t_innerHTML; 
				
				//Zeitmessung von innerText
				const t2 = performance.now();
				obj1.innerText = " Das ist ein Test ";
				const t3 = performance.now();
				let t_innerText = (t3 - t2);
				console.log(`Call to innerText took ${t_innerText} milliseconds.`);
				messungen[durchlauf][1] = t_innerText;
				
				//Zeitmessung von textContent
				const t4 = performance.now();
				obj1.textContent = " Das ist ein Test ";
				const t5 = performance.now();
				let t_textContent = (t5 - t4);
				console.log(`Call to textContent took ${t_textContent} milliseconds.`);
				messungen[durchlauf][2] = t_textContent;
	
				//Zeitmessung von outerHTML
				const t6 = performance.now();
				obj1.outerHTML = " Das ist ein Test ";
				const t7 = performance.now();
				const t_outerHTML = (t7 - t6);
				console.log(`Call to outerHTML took ${t_outerHTML} milliseconds.`);
				messungen[durchlauf][3] = t_outerHTML;
			}
            
			var tab = document.createElement("TABLE");
			tab.setAttribute("id", "messungenTabId");
			document.getElementById("U7A7_1").appendChild(tab);
			/*Tabellen Header erstellen mit Spalten
			Durchlauf:, HTMLElement properties:, Performanzmessungen in Millisekunden:*/
			var tabTrHeader = document.createElement("TR");
			tabTrHeader.setAttribute("id", "tabTrHeader");
			
			var durchlaufTH = document.createElement("TH");
			var durchlaufTHValue = document.createTextNode("Durchlauf:");
			durchlaufTH.appendChild(durchlaufTHValue);
			tabTrHeader.appendChild(durchlaufTH);
			
			var hTMLElementPropTH = document.createElement("TH");
			var hTMLElementPropTHValue = document.createTextNode("HTMLElement properties:");
			hTMLElementPropTH.appendChild(hTMLElementPropTHValue);
			tabTrHeader.appendChild(hTMLElementPropTH);
			
			var zeitTH = document.createElement("TH");
			var zeitTHValue = document.createTextNode("Performanzmessungen in Millisekunden:");
			zeitTH.appendChild(zeitTHValue);
			tabTrHeader.appendChild(zeitTH);
			
			document.getElementById("messungenTabId").appendChild(tabTrHeader);
			
			for( let i = 0; i < durchlaeufAnzahl; i++) {
				//Tabelle mit Werte befüllen:
				var tabTr0 = document.createElement("TR");
				tab.appendChild(tabTr0);
				var tabTr1 = document.createElement("TR");
				tab.appendChild(tabTr1);
				var tabTr2 = document.createElement("TR");
				tab.appendChild(tabTr2);
				var tabTr3 = document.createElement("TR");
				tab.appendChild(tabTr3);
				
				
				tabTr0.appendChild(document.createElement("TD")).appendChild( document.createTextNode(i));
				tabTr1.appendChild(document.createElement("TD")).appendChild( document.createTextNode(i));
				tabTr2.appendChild(document.createElement("TD")).appendChild( document.createTextNode(i));
				tabTr3.appendChild(document.createElement("TD")).appendChild( document.createTextNode(i));
				
				tabTr0.appendChild(document.createElement("TD")).appendChild( document.createTextNode("innerHTML"));
				tabTr1.appendChild(document.createElement("TD")).appendChild( document.createTextNode("innerText"));
				tabTr2.appendChild(document.createElement("TD")).appendChild( document.createTextNode("textContent"));
				tabTr3.appendChild(document.createElement("TD")).appendChild( document.createTextNode("outerHTML"));
				
				tabTr0.appendChild(document.createElement("TD")).appendChild( document.createTextNode(messungen[i][0]));
				tabTr1.appendChild(document.createElement("TD")).appendChild( document.createTextNode(messungen[i][1]));
				tabTr2.appendChild(document.createElement("TD")).appendChild( document.createTextNode(messungen[i][2]));
				tabTr3.appendChild(document.createElement("TD")).appendChild( document.createTextNode(messungen[i][3]));
			}
        </script>

    </div>
<?php include ("./includes/footer.php"); ?>