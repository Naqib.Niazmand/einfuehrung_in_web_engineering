<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
		<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			background-color: #f9f2f4;
			border-radius: 4px;;
		}
		</style>
        <h1>Beschreibung der Aufgabe</h1>
        <h2>7.3. TopSort als WebApp (optional)</h2>
				
        <div class="panel-heading"><p>Schreiben Sie eine Web-Oberfläche, in der man beliebige Beziehungen (Vorrang-Relationen) eingeben kann, für die dann die topologische Sortierung per Knopfdruck auf der Webseite ausgegeben wird.</p><p><span>Für die Eingabe können Sie </span><a target="_blank" href="https://www.w3schools.com/html/html_forms.asp" rel="noopener">HTML5-Eingabefelder</a><span> oder </span><a target="_blank" href="https://www.w3schools.com/tags/att_global_contenteditable.asp" rel="noopener">contentEditable</a><span> verwenden.</span></p></div>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <h2>7.3. TopSort als WebApp</h2>        
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="UTF-8"/>
			</head>
			<body>
				<button onclick="addInput()">Beziehung hinzufügen</button>
				<div id="input"></div>
				<button onclick="evaluateInput()">Topologische Sortierung durchführen</button>
				<div id="output"></div>
				<script src="TopSort_Script.js"></script>
			</body>
		</html>
		
    </div>
<?php include ("./includes/footer.php"); ?>