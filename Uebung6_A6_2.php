<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>6.2. Textanalyse mit filter-map-reduce</h2>
        <p>Schreiben Sie in JavaScript eine Textanalyse. Ermitteln Sie die häufigsten Begriffe im Text <a target="_blank" href="https://kaul.inf.h-brs.de/we/assets/html/plagiatsresolution.html" rel="noopener">Plagiatsresolution</a>. Filtern Sie dabei alle <a target="_blank" href="https://de.wikipedia.org/wiki/Stoppwort" rel="noopener">Stoppworte</a> und HTML-Tags. Reduzieren Sie das Ergebnis auf die 3 häufigsten Begriffe.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Üben Sie sich in Funktionaler Programmierung. Geben Sie Ihren Quellcode hier ein.</p>
        <p class="TextBlock">
		
	"use strict";<br>
    async function holeTextAusURL() {<br>
        &emsp; let url = 'https://kaul.inf.h-brs.de/we/assets/html/plagiatsresolution.html'; <br>
        &emsp; let response = await fetch(url);<br>
        &emsp; let responseText = await response.text();<br>
        &emsp; return responseText;<br>
    }<br>
    function entferneHtmlTags(str) {<br>
		&emsp; if((str===null) || (str==='')){<br>
		&emsp; 	&emsp; return false;<br>
		&emsp; }else{<br>
		&emsp; &emsp;	return str.toString().replace( /(<([^>]+)>)/ig, '');<br>
		&emsp; }<br>
    }<br>
    function ohneStoppWoerter (arg)  {<br>
        &emsp; let stoppWoerter =  ["der", "die", "das", "den", "des", "einer", "eine", "eines", "und", "oder", "doch", "weil", "an", "in", "von", "nicht"];<br>
        &emsp; return (stoppWoerter.find(wort => wort.includes(arg.toLowerCase())) === undefined);<br>
    }<br>
    function haeufigstenBegriffe(arg) {<br>
       &emsp; return Object.keys(arg).sort(function (a, b) {<br>
       &emsp;   &emsp;  return arg[a] - arg[b]<br>
       &emsp; });<br>
    }<br>
    (async() => {<br>
        &emsp; let alleWoerter = {};<br>
        &emsp; let textData = await holeTextAusURL();<br>
        &emsp; let textDataOhneHtmlTags = entferneHtmlTags(textData); <br>    
        &emsp; let textDataOhneZeilenumbrueche = textDataOhneHtmlTags.replace(/(\r\n|\n|\r)/gm, ""); <br>  
        &emsp; textDataOhneZeilenumbrueche.split(" ")<br>                                   
        &emsp;         &emsp;&emsp; .filter(x => x.trim())<br>                         
        &emsp;         &emsp;&emsp; .filter(ohneStoppWoerter)    <br>                   
        &emsp;         &emsp;&emsp; .forEach(wort => {    <br>                       
		 alleWoerter[wort] = alleWoerter[wort] ? alleWoerter[wort] + 1 : 1;<br>
        &emsp;         &emsp;&emsp; });<br>
        &emsp; let woerter = haeufigstenBegriffe(alleWoerter); <br>                
        &emsp; console.log("Reduzieren Sie das Ergebnis <br>
		&emsp; auf die 3 häufigsten Begriffe: "+ <br>
		&emsp; Object.values(woerter).slice(<br>
		&emsp; Object.values(alleWoerter).length-3)); <br>
		&emsp; document.write("Reduzieren Sie das Ergebnis<br>
		&emsp; auf die 3 häufigsten Begriffe: "+ <br>
		&emsp; Object.values(woerter).slice(<br>
		&emsp; Object.values(alleWoerter).length-3)); <br>
    })();<br>
		
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>