<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>1.4. HTML Wireframe</h2>
        <p>Gegeben ist folgendes HTML-Wireframe:</p>
         <img src="https://kaul.inf.h-brs.de/we/assets/img/wireframe01.jpg" class="Image" alt="WE HTML-Wireframe">
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">
		<!DOCTYPE html>
		<html>
		<head>
		<title>Ü1.2</title>
		</head>
		<body>
		
		<h2>Übung 1.2: Inventors of the Web</h2>
		<ul>
			<li><strong><mark>Tim Berners-Lee:</mark> WWW, HTTP, HTML, URI</strong></li>
			<li><strong>Hakom Lie and Bert Boss: CSS</strong></li>
			<li><strong>Brendan Eich: JavaScript</strong></li>
		</ul>
		<hr>
		<br>
		<h2> Inventors of the WWW </h2>
		<table style="width:100%", border="10">
			<tr> 
				<th colspan="4">
					<strong>Inventors of the www</strong>
				</th>
			</tr>
			<tr> 
				<th><strong>WWW</strong></th>
				<th><strong>HTML</strong></th>
				<th><strong>CSS</strong></th>
				<th><strong>JavaScript</strong></th>
			</tr>
			<tr>
			<td><mark><strong>Tim Berners-Lee</strong></mark></td>
			<td><mark><strong>Tim Berners-Lee</strong></mark></td>
			<td><strong>Hakom Lie and Bert Bos</strong></td>
			<td><strong>Brendan Eich</strong></td>
		</tr>
		</table>
		<br>
		<hr>
		
		<table>
			<tr> 
				<th colspan="3">
					<strong>Inventors of the www</strong>
					<hr>
				</th>
			</tr>
			<tr> 
				<th><strong>HTML</strong></th>
				<th><strong>|</strong></th>
				<th><strong>JavaScript</strong></th>
			</tr>
			<tr>
			<td><img src="https://kaul.inf.h-brs.de/we/assets/img/tbl.jpg" alt="Tim Berners-Lee"></td>
			<td>|</td>
			<td><img src="https://kaul.inf.h-brs.de/we/assets/img/eich.jpg" alt="Brendan Eich"></td>
		</tr>
		<tr> 
				<th><mark><strong>Tim Berners-Lee</strong></mark></th>
				<th><strong>|</strong></th>
				<th><strong>Brendan Eich</strong></th>
			</tr>
		</table>
		<br>
		<hr>
		</body>
		</html>
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>