<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>1.3. HTML-Literatur lesen und Fragen beantworten</h2>
        <p>Was ist HTML?</p>
        <p>Wie kann man eine geschachtelte geordnete Liste der Schachtelungstiefe 3 erzeugen?</p>
        <p>Wie ist eine HTML-Tabelle aufgebaut?</p>
        <p>Welche Konventionen sollte man bei Pfaden und Dateinamen beachten?</p>
        <p>Wie baut man in HTML ein Menü?</p>
        <p>Welche Attribute sollte man bei Bildern wie verwenden?</p>
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Was ist HTML?</p>
        <p class="TextBlock">HTML steht für Hypertext Markup Language. Das ist Englisch und die Übersetzung ins Deutsche verrät uns, dass es sich um eine Hypertext-Auszeichnungssprache handelt. Doch was bedeutet das? Nun, Auszeichnungssprache bedeutet, dass es sich hier um eine Sprache handelt, die von einer Maschine gelesen werden kann und zur Gliederung und Formatierung von Texten und anderen Daten dient. Hypertext bedeutet, dass diese Texte auch Querverweise, sogenannte Hyperlinks, enthalten können, durch die man direkt zu anderen Textstellen im selben oder in anderen Hypertexten springen kann.</p>
        <p class="description">Wie kann man eine geschachtelte geordnete Liste der Schachtelungstiefe 3 erzeugen?</p>
        <p class="TextBlock">
			Beispiel für eine geordnete Liste in HTML
			<ol class="TextBlock">
				<li>Schachtelungstiefe 1</li>
				<ol>
					<li>Schachtelungstiefe 2</li>
					<ol>
						<li>Schachtelungstiefe 3</li>
					</ol>
				</ol> 
			</ol> 
		</p>
        <p class="description">Wie ist eine HTML-Tabelle aufgebaut?</p>
		<table class="TextBlock">
			<tr>
				<th>Vorname</th>
				<th>Nachmame</th>
				<th>Matr. Nr</th>
			</tr>
			<tr>
				<th>Naqib</th>
				<th>Niazmand</th>
				<th>01234567890000000000</th>
			</tr>
			<tr>
				<th>Max</th>
				<th>Mustermann</th>
				<th>99999999999999900</th>
			</tr>
		</table>	
        <p class="description">Welche Konventionen sollte man bei Pfaden und Dateinamen beachten?</p>
        <p class="TextBlock">
			Ordnerstruktur:<br>
			-home<br>
			-html (ordner)<br>
			-img (ordner)<br>
			-index.html<br>
			Für Dateinamen:<br>
			-Kleinbuchstaben verwenden<br>
			-Sonderzeichen vermeiden<br>
			-unterstriche statt leereichen
		</p>
        <p class="description">Wie baut man in HTML ein Menü?</p>
			<nav class="TextBlock">
				<ul>
					<li><a href="index.php">Mit einem < nav > Tag in Kombination mit Liste + Links.</a></li>
				</ul>
			</nav>
        <p class="description">Welche Attribute sollte man bei Bildern wie verwenden?</p>
        <p class="TextBlock">
			alt für den fall dass, das Bild nicht angezeigt wird.<br><br>
			<img src="HomerSimpson.jpg" class="Image" alt="Homer Simpson">
			<br><br>
			Bild Quelle aus Google Link: 
			<a href="https://www.google.de/search?q=homer+simpson+n%C3%A4chstes+wochenende+ist+wieder+wochenende&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiuwMjih8fuAhXwwAIHHQM3B_YQ_AUoAXoECBEQAw&biw=1360&bih=568#imgrc=gEAZpvTiXQ4xCM">homer simpson nächstes wochenende ist wieder wochenende</a>
			
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>