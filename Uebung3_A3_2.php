<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
        <div class="task">
            <h1>Beschreibung der Aufgabe</h1>
            <h2>3.2. Responsiv mit Grid Mobile-First</h2>
            <p>Spielen Sie zunächst das <a target="_blank" href="https://cssgridgarden.com/#de" rel="noopener">Grid Garden </a> -Spiel, um Grid Layout zu lernen.<br><br>
			 Implementieren Sie dann das gleiche responsive Webdesign wie in Aufgabe 3.1 allerdings mit Grid und der Mobile-First-Strategie! Vermeiden Sie diesmal außerdem das Erscheinen von Scrollbars.</p>
            <img alt="Responsiv mit Flexbox Mobile-First-Strategie!" src="https://kaul.inf.h-brs.de/we/assets/img/holy-grail1.png" width="100%">
			<p>Geben Sie hier HTML- und CSS-Code zusammen ein: Siehe Rechts Lösungsseite</p>	
        </div>
        <div class="solution">
			<h1>Lösung der Aufgabe</h1>
			<p class="description">Implementieren Sie dann das gleiche responsive Webdesign wie in Aufgabe 3.1 allerdings mit Grid und der Mobile-First-Strategie! Vermeiden Sie diesmal außerdem das Erscheinen von Scrollbars.</p>
			
			<style>
			.solutionBodyForMobileFirstStrategie {
				background-color: #F2F2F2;
				
				display: grid;
			grid-template-columns: 100%;
			grid-template-rows:  14vh 14vh 55vh 14vh;
	
			grid-template-areas: 
			"rotHeaderSub"
			"gruenMenuSub"
			"blauContentSub"
			"pinkAsideSub";
			}
			
			.rotHeader{
            grid-area: rotHeaderSub;
            background-color:rgb(255,37,0);
        }

        .gruenMenu{
            grid-area: gruenMenuSub;
            background-color:rgb(44,238,39);
        }

        .blauContent{
            grid-area: blauContentSub;
            background-color:rgb(5,51,255);
        }

        .pinkAside{
            grid-area: pinkAsideSub;
            background-color:rgb(234,62,254);
        }
		
		/*Responsive Design für Mobile Ansicht
		nach Mobile-First-Strategie!*/
		@media(min-width: 400px){
            .solutionBodyForMobileFirstStrategie{
				grid-template-columns: 30% 70%;
				grid-template-rows:  14vh 68vh 14vh;
	
				grid-template-areas: 
				"rotHeaderSub rotHeaderSub"
				"gruenMenuSub blauContentSub"
				"pinkAsideSub pinkAsideSub";
            }

            .rotHeader, .gruenMenu, .blauContent, .pinkAside{
                margin: .25em;
            }

        }
		/*Responsive Design für Tablet Ansicht*/
		@media(min-width: 650px){
			.solutionBodyForMobileFirstStrategie{
				grid-template-columns: 15% 70% 15%;
				grid-template-rows:  14vh 82vh;
	
				grid-template-areas: 
				"rotHeaderSub rotHeaderSub rotHeaderSub"
				"gruenMenuSub blauContentSub pinkAsideSub"
			}
	
			.rotHeader, .gruenMenu, .blauContent, .pinkAside{
				margin: .25em;
			}
        }
			</style>
			<div class="solutionBodyForMobileFirstStrategie">
				<div class="rotHeader">Header</div>  
				<div class="gruenMenu">Menu</div>
				<div class="blauContent">Content</div>
				<div class="pinkAside">Aside</div>
			</div>
			
        </div>
<?php include ("./includes/footer.php"); ?>
