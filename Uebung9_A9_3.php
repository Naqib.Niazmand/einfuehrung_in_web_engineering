<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 9.3: Vue.js WWW-Navigator</h2>
        <p><span>Schreiben Sie Ihren WWW-Navigator als SPA in Vue.js </span><s><span>mit </span><a target="_blank" href="https://router.vuejs.org/guide/" rel="noopener">Vue Router</a><span> und/oder mit </span><a target="_blank" href="https://vuex.vuejs.org/" rel="noopener">Vuex</a><span> als State Manager</span></s><span>. </span></p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="TextBlock">Aus Zeitlichen Gründen leider nicht machbar</p>
    </div>
<?php include ("./includes/footer.php"); ?>