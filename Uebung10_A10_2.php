<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 10.2: Login mit PHP</h2>
        <p>Schreiben Sie eine sichere PHP-Lösung für Login, das die persistierten Registrierungsdaten aus der letzten Aufgabe nutzt.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <?php include ("./einloggen.html"); ?>
    </div>
<?php include ("./includes/footer.php"); ?>