<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>

    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>1.2. HTTP</h2>
        <p>Sie bekommen im Browser den HTTP Status Code 200. Was bedeutet das?</p>
        <p>Sie bekommen im Browser den HTTP Status Code 301. Was hat das zu bedeuten?</p>
        <p>Sie bekommen im Browser den HTTP Status Code 400. Was hat das zu bedeuten? Was können Sie dagegen tun?</p>
        <p>Sie bekommen im Browser den HTTP Status Code 403. Was hat das zu bedeuten? Was können Sie dagegen tun?</p>
        <p>In einer Webanwendung benötigen Sie eine OPTIONS-Anfrage, die die Optionen des Servers vor dem eigentlichen Zugriff erfragen soll. Aus Performanzgründen soll die Abfrage jedoch cacheable sein. Wie könnten Sie dafür eine Lösung angehen?</p>
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Sie bekommen im Browser den HTTP Status Code 200. Was bedeutet das?</p>
        <p class="TextBlock">Der HTTP-Statuscode 200 OK gibt an, dass eine Anfrage erfolgreich verlaufen ist. Eine 200-Antwort ist standardmäßig im Cache speicherbar.</p>
        <p class="description">Sie bekommen im Browser den HTTP Status Code 301. Was hat das zu bedeuten?</p>
        <p class="TextBlock">Der Status Code 301 sagt aus, dass die vom Client angeforderte Ressource nicht länger unter der angegebenen Adresse erreichbar ist, sondern dauerhaft auf eine andere Adresse verschoben wurde (Redirect).</p>
        <p class="description">Sie bekommen im Browser den HTTP Status Code 400. Was hat das zu bedeuten? Was können Sie dagegen tun?</p>
        <p class="TextBlock">Der HTTP-Statuscode 400 Bad Request gibt an, dass der Server die Anfrage nicht verarbeiten kann, weil anscheinend ein clientseitiger Fehler geschehen ist (z.B. eine syntaktisch falsche Anfrage). Der Client sollte diese Anfrage nicht unmodifiziert wiederholen.</p>
        <p class="description">Sie bekommen im Browser den HTTP Status Code 403. Was hat das zu bedeuten? Was können Sie dagegen tun?</p>
        <p class="TextBlock">Der HTTP 403 Forbidden Statuscode zeigt an, dass der Server die Anfrage zwar verstanden hat, aber sich weigert, sie zuzulassen. Dieser Status ist 401 ähnlich, aber ein erneutes Anmelden wird am Ergebnis nichts ändern.</p>
        <p class="description">In einer Webanwendung benötigen Sie eine OPTIONS-Anfrage, die die Optionen des Servers vor dem eigentlichen Zugriff erfragen soll. Aus Performanzgründen soll die Abfrage jedoch cacheable sein. Wie könnten Sie dafür eine Lösung angehen?</p>
        <p class="TextBlock">Optionsanfragen Cachen wie z.B. Prokoll und Version, Verschlüsselte Verbindung und welche Broswer supportet werden.</p>
    </div>

<?php include ("./includes/footer.php"); ?>