<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
        <h1>Beschreibung der Aufgabe</h1>
        <h2>5.5. DeepCopy</h2>
        <p>Schreiben Sie eine rekursive Funktion deepCopy( struct ) als ES6-Ausdruck, so dass beliebig geschachtelte Arrays und Objekte struct tiefenkopiert werden können. Verwenden Sie zu diesem Zweck den <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator" rel="noopener">konditionalen ternären Operator</a>, <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map" rel="noopener">Array.map()</a>, <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/fromEntries" rel="noopener">Object.fromEntries()</a> und <a target="_blank" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries" rel="noopener">Object.entries()</a>. Verwenden Sie dabei nur Arrow Functions und Ausdrücke, keine Anweisungen, keine Blöcke. Verwenden Sie nicht die JSON-Methoden.</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <p class="description">Geben Sie hier Ihren ECMAScript-Code inkl. Tests ein. Verwenden Sie für Ihre Tests die Funktion <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">console.assert</a>.</p>
        <p class="TextBlock">

//Lösungshinweise zu Aufgabe 5.5. aus <a href="https://developer.mozilla.org/de/docs/Web/API/Console/assert" rel="noopener">https://kaul.inf.h-brs.de/we/#app-content-6-3t</a> übernommen<br><br>		
const x = { a: 1, b: 2, c: [ { d: 3 }, 4 ] }; <br><br>

const deepCopy = ( struct ) =><br>
  Array.isArray( struct ) ? struct.map( item => deepCopy( item ) ) :<br>
    typeof struct === "object" && struct !== null ?<br>
	Object.fromEntries( Object.entries( struct ).map( <br>
	([key, value]) => [ key, deepCopy( value ) ] ) ) : struct;<br><br>

// Deep equal<br>
// https://stackoverflow.com/questions/25456013/javascript-deepequal-comparison/25456134 <br><br>
const deepEqual = (x, y) => {<br>
  if (x === y) {<br>
  &emsp;  return true;<br>
  }<br>
  else if ((typeof x == "object" && x != null) <br>
  && (typeof y == "object" && y != null)) {<br>
    if (Object.keys(x).length !== Object.keys(y).length)<br>
    &emsp;  return false;<br>

    for (var prop in x) {<br>
    &emsp;  if (y.hasOwnProperty(prop))<br>
    &emsp;  {<br>
    &emsp; &emsp;   if (! deepEqual(x[prop], y[prop]))<br>
    &emsp;   &emsp;   return false;<br>
    &emsp;  }<br>
    &emsp;  else<br>
    &emsp; &emsp;   return false;<br>
    }<br>

    return true;<br>
  }<br>
  else<br>
   &emsp; return false;<br>
};<br>

console.log( deepCopy( x ) );<br>
console.assert( deepEqual( x, deepCopy( x )) );<br>
console.assert( deepEqual( x, deepCopy( x )) );<br>
		
		</p>
    </div>
<?php include ("./includes/footer.php"); ?>