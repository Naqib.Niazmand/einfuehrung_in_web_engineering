<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
	<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			background-color: #f9f2f4;
			border-radius: 4px;;
		}
	</style>
        <h1>Beschreibung der Aufgabe</h1>
        <h2>Aufgabe 6.1: Funktionen in JavaScript</h2>
		
        <p>Schreiben Sie eine Funktion <code>identity_function()</code>, die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt.</p>
		
		<p>Schreiben Sie eine Addier-Funktion <code>addf()</code>, so dass <code>addf(x)(y)</code> genau <code>x + y</code> zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. <code>addf(x)</code> liefert eine Funktion, die auf <code>y </code>angewandt wird.)</p>
		
		<p>Schreiben Sie eine Funktion <code>applyf()</code> , die aus einer binären Funktion wie <code>add(x,y)</code>  eine Funktion <code>addf </code> berechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. <code>addf = applyf(add); addf(x)(y)</code>  soll <code>add(x,y)</code>  liefern. Entsprechend <code>applyf(mul)(5)(6)</code>  soll <code>30 </code> liefern, wenn <code>mul</code>  die binäre Multiplikation ist.</p>
		
		<p>Schreiben Sie eine Funktion <code>curry()</code> (von Currying), die eine binäre Funktion und ein Argument nimmt, um daraus eine Funktion zu erzeugen, die ein zweites Argument entgegen nimmt, z.B. <code>add3 = curry(add, 3);add3(4)</code> ergibt <code>7</code>. <code>curry(mul, 5)(6)</code> ergibt <code>30</code>.</p>
		
		<p>Erzeugen Sie die <code>inc</code>-Funktion mit Hilfe einer der Funktionen <code>addf, applyf</code> und <code>curry</code> aus den letzten Aufgaben, ohne die Funktion <code>inc()</code> selbst zu implementieren. (<code>inc(x) </code>soll immer <code>x + 1 </code>ergeben und lässt sich natürlich auch direkt implementieren. Das ist aber hier nicht die Aufgabe.) Vielleicht schaffen Sie es auch, drei Varianten der <code>inc()</code>-Implementierung zu schreiben?</p>
		
		<p>Schreiben Sie eine Funktion <code>methodize()</code>, die eine binäre Funktion (z.B. add, mul) in eine unäre Methode verwandelt. Nach Number.prototype.add = methodize(add); soll (3).add(4) genau 7 ergeben.</p>
		
		<p>Schreiben Sie eine Funktion <code>demethodize()</code>, die eine unäre Methode (z.B. add, mul) in eine binäre Funktion umwandelt. demethodize(Number.prototype.add)(5, 6) soll 11 ergeben.</p>
		
		<p>Schreiben Sie eine Funktion <code>twice()</code>, die eine binäre Funktion in eine unäre Funktion umwandelt, die den einen Parameter zweimal weiter reicht. Z.B. var double = twice(add); double(11) soll 22 ergeben; var square = twice(mul); square(11) soll mul(11,11) === 121 ergeben.</p>
		
		<p>Schreiben Sie eine Funktion <code>composeu()</code>, die zwei unäre Funktionen in eine einzelne unäre Funktion transformiert, die beide nacheinander aufruft, z.B. soll composeu(double, square)(3) genau 36 ergeben.</p>
		
		<p>Schreiben Sie eine Funktion <code>composeb()</code>, die zwei binäre Funktionen in eine einzelne Funktion transformiert, die beide nacheinander aufruft, z.B. composeb(add, mul)(2, 3, 5) soll 25 ergeben.</p>
		
		<p>Schreiben Sie eine Funktion <code>once()</code>, die einer anderen Funktion nur einmal erlaubt, aufgerufen zu werden, z.B. add_once = once(add); add_once(3, 4) soll beim ersten Mal 7 ergeben, beim zweiten Mal soll jedoch add_once(3, 4) einen Fehlerabbruch bewirken.</p>
		
		<p>Schreiben Sie eine Fabrik-Funktion <code>counterf()</code>, die zwei Funktionen inc() und dec() berechnet, die einen Zähler hoch- und herunterzählen. Z.B. counter = counterf(10); Dann soll counter.inc() 11 und counter.dec() wieder 10 ergeben.</p>
		
		<p>Schreiben Sie eine rücknehmbare Funktion <code>revocable()</code>, die als Parameter eine Funktion nimmt und diese bei Aufruf ausführt. Sobald die Funktion aber mit <code>revoke()</code> zurück genommen wurde, führt ein erneuter Aufruf zu einem Fehler. Z.B.<br>
		<code>
		temp = revocable(alert);<br>
		temp.invoke(7); // führt zu alert(7);<br>
		temp.revoke();<br>
		temp.invoke(8); // Fehlerabbruch!<br>
		</code>
		</p>
		
		<p>Implementieren Sie ein "Array Wrapper"-Objekt mit den Methoden get, store und append, so dass ein Angreifer keinen Zugriff auf das innere, private Array hat.<br>
		<code>
		my_vector = vector();<br>
		my_vector.append(7);<br>
		my_vector.store(1, 8);<br>
		my_vector.get(0) // 7<br>
		my_vector.get(1) // 8<br>
		</code>
		
		</p>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        
		<p class="description">Schreiben Sie eine Funktion <code>identity_function()</code>, die ein Argument als Parameter entgegen nimmt und eine Funktion zurück gibt, die dieses Argument zurück gibt.</p>
        <p class="TextBlock">
		function identity_function( x ){<br>
		&emsp;	return function(){<br>
		&emsp;&emsp;		return x;<br> 
		&emsp;	}<br> 
		}<br>
		</p>
		
		<p class="description">Schreiben Sie eine Addier-Funktion <code>addf()</code>, so dass <code>addf(x)(y)</code> genau <code>x + y</code> zurück gibt. (Es haben also zwei Funktionsaufrufe zu erfolgen. <code>addf(x)</code> liefert eine Funktion, die auf <code>y </code>angewandt wird.)</p>
        <p class="TextBlock">
		function addf (x) {<br>
		&emsp;	return function (y) {<br>
		&emsp;&emsp;		return x+y;<br>
		&emsp;	}<br>
		}<br>
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>applyf()</code> , die aus einer binären Funktion wie <code>add(x,y)</code>  eine Funktion <code>addf </code> berechnet, die mit zwei Aufrufen das gleiche Ergebnis liefert, z.B. <code>addf = applyf(add); addf(x)(y)</code>  soll <code>add(x,y)</code>  liefern. Entsprechend <code>applyf(mul)(5)(6)</code>  soll <code>30 </code> liefern, wenn <code>mul</code>  die binäre Multiplikation ist.</p>
        <p class="TextBlock">
		
		function add(x,y) {<br>
		&emsp;	return x+y;<br>
		}<br>
		<br>
		function mul(x,y) {<br>
		&emsp;	return x*y;<br>
		}<br>
		<br>
		function applyf(f){<br>
		&emsp;	return function(x){<br>
		&emsp;&emsp;		return function(y){<br>
		&emsp;&emsp;&emsp;			return f(x,y);<br>
		&emsp;&emsp;		}<br>
		&emsp;	}<br>
		}<br>
		
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>curry()</code> (von Currying), die eine binäre Funktion und ein Argument nimmt, um daraus eine Funktion zu erzeugen, die ein zweites Argument entgegen nimmt, z.B. <code>add3 = curry(add, 3);add3(4)</code> ergibt <code>7</code>. <code>curry(mul, 5)(6)</code> ergibt <code>30</code>.</p>
        <p class="TextBlock">
		
		function curry(f, x){<br>
		&emsp;	return function(y){<br>
		&emsp;&emsp;		return f(x,y);<br>
		&emsp;	}<br>
		}<br>
		
		</p>
		
		<p class="description">Erzeugen Sie die <code>inc</code>-Funktion mit Hilfe einer der Funktionen <code>addf, applyf</code> und <code>curry</code> aus den letzten Aufgaben, ohne die Funktion <code>inc()</code> selbst zu implementieren. (<code>inc(x) </code>soll immer <code>x + 1 </code>ergeben und lässt sich natürlich auch direkt implementieren. Das ist aber hier nicht die Aufgabe.) Vielleicht schaffen Sie es auch, drei Varianten der <code>inc()</code>-Implementierung zu schreiben?</p>
        <p class="TextBlock">
		
		function add(x, y){<br>
		&emsp;	return x+y;<br>
		}<br>
		<br>
		function curry(f, x){<br>
		&emsp;	return function(y){<br>
		&emsp;&emsp;		return f(x,y);<br>
 		&emsp;	}<br>
		}<br>
		<br>
		var inc = curry(add, 1);<br>
		
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>methodize()</code>, die eine binäre Funktion (z.B. add, mul) in eine unäre Methode verwandelt. Nach Number.prototype.add = methodize(add); soll (3).add(4) genau 7 ergeben.</p>
        <p class="TextBlock">
		
		function add(x, y) {<br>
		&emsp;	return x+y;<br>
		}<br>
		function mul(x, y) {<br>
		&emsp;	return x*y;<br>
		}<br>
		
		function methodize(f){<br>
		&emsp;	return function(y){<br>
		&emsp;&emsp;		return f(this,y);<br>
		&emsp;	}<br>
		}<br>
		
		//Testen<br>
		function PrintOnDocument() {<br>
		&emsp;	Number.prototype.add = methodize(add); <br>
		&emsp;	Number.prototype.mul = methodize(mul);<br>
		&emsp;	document.write("Test methodize add(3,4): 7 =  " +(3).add(4));<br>
		&emsp;	document.write("Test methodize mul(3,4): 12 =  " +(3).mul(4));<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
		
		<p class="description">Schreiben Sie eine Funktion <code>demethodize()</code>, die eine unäre Methode (z.B. add, mul) in eine binäre Funktion umwandelt. demethodize(Number.prototype.add)(5, 6) soll 11 ergeben.</p>
        <p class="TextBlock">
		function demethodize(f) {<br>
			&emsp; return function(x,y) {<br>
			&emsp; 	&emsp; return f.call(x,y);<br>
			&emsp; };<br>
		}<br>
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>twice()</code>, die eine binäre Funktion in eine unäre Funktion umwandelt, die den einen Parameter zweimal weiter reicht. Z.B. var double = twice(add); double(11) soll 22 ergeben; var square = twice(mul); square(11) soll mul(11,11) === 121 ergeben.</p>
        <p class="TextBlock">
		
		function twice(f) {<br>
		&emsp;	return function(y){<br>
		&emsp;&emsp;		return f(y,y);<br>
		&emsp;	};<br>
		}<br>
		//Testen <br>
		function PrintOnDocument() {<br>
		&emsp; var double = twice(add); <br>
		&emsp; document.write("Test twice add");<br>
		&emsp; document.write("double(11) soll 22 ergeben:<br>
		&emsp; 22 =  " +double(11));<br>
		&emsp; var square = twice(mul);<br>
		&emsp; document.write("Test twice mul");<br>
		&emsp; document.write("square(11) soll mul(11,11) === 121 ergeben:<br> 
		&emsp; 121 =  " +square(11));<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>composeu()</code>, die zwei unäre Funktionen in eine einzelne unäre Funktion transformiert, die beide nacheinander aufruft, z.B. soll composeu(double, square)(3) genau 36 ergeben.</p>
        <p class="TextBlock">
		
		//g(f(x))<br>
		function composeu(f,g){ <br>
			return function(x){<br>
				return g(f(x));<br>
			};<br>
		}<br>
		//Testen<br>
		function PrintOnDocument() {<br>
		&emsp; var double = twice(add);<br> 
		&emsp; var square = twice(mul); <br>
		&emsp; document.write("Test composeu ");<br>
		&emsp; document.write("<br>");<br>
		&emsp; document.write("composeu(double, square)(3) genau 36 ergeben:<br>
		&emsp; 36 =  " + composeu(double, square)(3));<br>
		}<br>
		PrintOnDocument();<br>
		<br>
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>composeb()</code>, die zwei binäre Funktionen in eine einzelne Funktion transformiert, die beide nacheinander aufruft, z.B. composeb(add, mul)(2, 3, 5) soll 25 ergeben.</p>
        <p class="TextBlock">
		
		function composeb(fx, fy){<br>
		&emsp;	return function(x, y, z){<br>
		&emsp;&emsp;		return fy(fx(x,y), z);<br>
		&emsp;	}<br>
		}<br>
		</p>
		
		<p class="description">Schreiben Sie eine Funktion <code>once()</code>, die einer anderen Funktion nur einmal erlaubt, aufgerufen zu werden, z.B. add_once = once(add); add_once(3, 4) soll beim ersten Mal 7 ergeben, beim zweiten Mal soll jedoch add_once(3, 4) einen Fehlerabbruch bewirken.</p>
        <p class="TextBlock">
		
		function once(fn) { <br>
			&emsp; var result;<br>
			&emsp; return function() { <br>
			&emsp; 	&emsp; if(fn) {<br>
			&emsp; 	&emsp; 	&emsp; result = fn.apply(this, arguments);<br>
			&emsp; 	&emsp; 	&emsp; fn = null;<br>
			&emsp; 	&emsp; }else{<br>
			&emsp; 	&emsp; 	&emsp;result = "Fehlerabbruch nur einmal aufrufbar!";<br>
			&emsp; 	&emsp; }<br>
			&emsp; 	&emsp; return result;<br>
			&emsp; };<br>
		}<br>
		// Testen <br>
		function PrintOnDocument() {<br>
			&emsp; add_once = once(add); <br>
			&emsp; document.write("Test once ");<br>
			&emsp; document.write("add_once(3, 4): 7 =  " + add_once(3, 4));<br>
			&emsp; document.write("Test once 2 aufruf ");<br>
			&emsp; document.write("add_once(8, 8): Fehler =  " + add_once(8, 8));<br>
			&emsp; document.write("Test once 3 aufruf ");<br>
			&emsp; document.write("add_once(9, 9): Fehler =  " + add_once(9, 9));<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
		<p class="description">Schreiben Sie eine Fabrik-Funktion <code>counterf()</code>, die zwei Funktionen inc() und dec() berechnet, die einen Zähler hoch- und herunterzählen. Z.B. counter = counterf(10); Dann soll counter.inc() 11 und counter.dec() wieder 10 ergeben.</p>
        <p class="TextBlock">
		
		function counterf(v){<br>
			&emsp; return {<br>
			&emsp; 	&emsp; count: v,<br>
			&emsp; 	&emsp; inc: function() {<br>
			&emsp; 	&emsp; &emsp; return ++this.count;<br>
			&emsp; 	&emsp; },<br>
			&emsp; 	&emsp; dec: function() {<br>
			&emsp; 	&emsp; 	&emsp; return --this.count;<br>
			&emsp; 	&emsp; }<br>
			&emsp; };<br>
		}<br>
		//Testen<br>
		function PrintOnDocument() {<br>
			&emsp; counter = counterf(10); <br>
			&emsp; document.write("Test counterf(10) ");<br>
			&emsp; document.write("counter.inc(): 11 =  " + counter.inc());<br>
			&emsp; document.write("counter.dec(): 10 =  " + counter.dec());<br>
			&emsp; document.write("counter.dec(): 9 =  " + counter.dec());<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
		<p class="description">
		
		Schreiben Sie eine rücknehmbare Funktion <code>revocable()</code>, die als Parameter eine Funktion nimmt und diese bei Aufruf ausführt. Sobald die Funktion aber mit <code>revoke()</code> zurück genommen wurde, führt ein erneuter Aufruf zu einem Fehler. Z.B.<br>
		<code>
		temp = revocable(alert);<br>
		temp.invoke(7); // führt zu alert(7);<br>
		temp.revoke();<br>
		temp.invoke(8); // Fehlerabbruch!<br>
		</code>
		
		</p>
        <p class="TextBlock">
		
		function revocable(f) {<br>
			&emsp; var revoked = false;<br>
			&emsp; var that = this;<br>
			&emsp; return {<br>
			&emsp; 	&emsp; invoke: function() {<br>
			&emsp; 	&emsp; 	&emsp; if(! revoked) {<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; return f.apply(that, arguments);<br>
			&emsp; 	&emsp; 	&emsp; } else {<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; console.log("Fehlerabbruch wurde<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; &emsp; &emsp; zurück genommen!");<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; document.write("Fehlerabbruch wurde<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; &emsp; &emsp; zurück genommen!");<br>
			&emsp; 	&emsp; 	&emsp; 	&emsp; throw('Fehlerabbruch wurde zurück genommen!');<br>
			&emsp; 	&emsp; 	&emsp; }<br>
			&emsp; 	&emsp; },<br>
			&emsp; 	&emsp; revoke: function() {<br>
			&emsp; 	&emsp; 	&emsp; revoked = true;<br>
			&emsp; 	&emsp; }<br>
			&emsp; };<br>
		}<br>
		//Testen<br>
		function PrintOnDocument() {<br>
			&emsp; temp = revocable(alert);<br>
			&emsp; document.write("Test revocable(alert) ");<br>
			&emsp; temp.invoke(7);<br>
			&emsp; temp.revoke();<br>
			&emsp; document.write(" temp.revoke() ");<br>
			&emsp; document.write("temp.invoke(8); =  " + temp.invoke(8));<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
		<p class="description">Implementieren Sie ein "Array Wrapper"-Objekt mit den Methoden get, store und append, so dass ein Angreifer keinen Zugriff auf das innere, private Array hat.<br>
		<code>
		my_vector = vector();<br>
		my_vector.append(7);<br>
		my_vector.store(1, 8);<br>
		my_vector.get(0) // 7<br>
		my_vector.get(1) // 8<br>
		</code>
		</p>
        <p class="TextBlock">
		
		function vector() {<br>
		&emsp;var array = [];<br>
		&emsp;return {<br>
		&emsp;	&emsp; get: function get(i) {<br>
		&emsp;	&emsp; 	&emsp; return array[i];<br>
		&emsp;	&emsp; },<br>
		&emsp;	&emsp; store: function store(i, v) {<br>
		&emsp;	&emsp; 	&emsp; array[i] = v;<br>
		&emsp;	&emsp; },<br>
		&emsp;	&emsp; append: function append(v) {<br>
		&emsp;	&emsp; 	&emsp; array.push(v);<br>
		&emsp;	&emsp; }<br>
		&emsp;};<br>
		}<br>
		//Testen<br>
		function PrintOnDocument() {<br>
		&emsp; my_vector = vector();<br>
		&emsp; my_vector.append(7);<br>
		&emsp; document.write("my_vector.append(7)");<br>
		&emsp; my_vector.store(1, 8);<br>
		&emsp; document.write("my_vector.store(1, 8);");<br>
		&emsp; document.write(" my_vector.get(0): 7 = "+my_vector.get(0));<br>
		&emsp; document.write(" my_vector.get(1): 8 = "+my_vector.get(1));<br>
		}<br>
		PrintOnDocument();<br>
		
		</p>
		
    </div>
<?php include ("./includes/footer.php"); ?>