<?php include ("./includes/header.inc.php"); ?>
<?php include ("./includes/breadcrumbNavigation.php"); ?>
<?php include ("./includes/menu.php"); ?>
    <div class="task">
		<style>
		code {
			padding: 2px 4px;
			font-size: 90%;
			color: #c7254e;
			background-color: #f9f2f4;
			border-radius: 4px;;
		}
		</style>
        <h1>Beschreibung der Aufgabe</h1>
        <h2>7.2. Rednerliste mit Zeitmessung</h2>		
		<div class="panel-heading"><p><span>Implementieren Sie die interaktive Anwendung "Rednerliste mit Zeitmessung" selbstständig in JavaScript durch Nutzung der </span><a target="_blank" href="https://www.w3schools.com/js/js_htmldom.asp" rel="noopener">DOM API</a><span> und der </span><a target="_blank" href="https://www.w3schools.com/js/js_timing.asp" rel="noopener">Timer-Funktionen</a><span>. Neue Redner sollen auf Knopfdruck hinzugefügt werden können. Deren Uhr wird dann sofort automatisch gestartet und alle anderen Uhren angehalten. Bei jedem Redner soll die individuelle, gemessene Redezeit sekundengenau angezeigt werden. Für jeden Redner soll es einen eigenen Start-/Stopp-Button geben. Es soll immer nur eine Uhr laufen. Angezeigt werden sollen die bisherigen Summenzeiten aller Redebeiträge der betreffenden Person. Suchen Sie eine möglichst kurze und elegante Lösung. Achten Sie gleichzeitig auf gute Usability: z.B. wenn die Eingabe mit einem Return beendet wird, soll der Button-Click nicht mehr erforderlich sein, usw.</span></p><div class="center"><img src="https://kaul.inf.h-brs.de/we/assets/img/Rednerliste.png" alt="Rednerliste" width="80%" height="auto"></div></div>
        
    </div>
    <div class="solution">
        <h1>Lösung der Aufgabe</h1>
        <h2>7.2. Rednerliste mit Zeitmessung</h2>
	   <style>
		label {
			padding-right: .8em;
			padding-bottom: .8em;
			font-family: "Arial Black", "Arial Bold", Gadget, sans-serif; 
		}
		
		button {
			font-family: "Arial Black", "Arial Bold", Gadget, sans-serif;
		}
		li {
			padding-bottom: .8em;
		}
		</style>
		<form id="RednerForm">
            <label for="NameRedner">Neuer Redner:</label>
            <input type="text" id="NameRedner" placeholder="Name">
            <button id="AddButton" type="submit" style="background-color: white; border: 3px thin black;">Hinzufügen</button>
        </form>
		<br>
        <ul id="rednerliste"> </ul>
	   <script src="RednerListe.js"></script>
    </div>
<?php include ("./includes/footer.php"); ?>